package gui.mvc.voting;

import gui.mvc.voting.controller.voting.AddCandidateTextField;
import gui.mvc.voting.controller.voting.VoteIncrementButton;
import gui.mvc.voting.controller.voting.VoteSetActionListener;
import gui.mvc.voting.model.PollData;
import gui.mvc.voting.model.interfaces.IPoll;
import gui.mvc.voting.model.interfaces.IPollChangeListener;
import gui.mvc.voting.view.voting.BarView;
import gui.mvc.voting.view.voting.CakeView;
import gui.mvc.voting.view.voting.LabelView;
import gui.mvc.voting.view.voting.TextfieldView;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 * Hauptklasse zur Aufgabe "VotingModel".
 * 
 * @author Patrick Fries
 */
@SuppressWarnings("serial")
public class VotingFrame extends JFrame
{
    /**
     * Initialisiert ein neues VotingFrame-Objekt mit den übergebenen
     * Argumenten.
     * 
     * @param question
     *            Frage, über die abgestimmt werden soll.
     * @param votingModel
     *            Modell, welches die zur Verfügung stehenden Kandidaten nebst
     *            ihren Stimmen verwaltet.
     */
    public VotingFrame(final String question, final IPoll votingModel)
    {
        super("Meinungsumfrage: " + question);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        final JPanel questionAndAddCandidatePanel = new JPanel(new GridLayout(1, 0, 10, 10));
        questionAndAddCandidatePanel.add(new QuestionPanel(question));
        questionAndAddCandidatePanel.add(new AddCandidatePanel(votingModel));

        final JPanel incrementAndSetPanel = new JPanel(new GridLayout(1, 0, 10, 10));
        incrementAndSetPanel.add(new IncrementPanel(votingModel));
        incrementAndSetPanel.add(new SetPanel(votingModel));

        final JPanel cakeAndBarPanel = new JPanel(new GridLayout(1, 0, 10, 10));
        cakeAndBarPanel.add(new CakePanel(votingModel));
        cakeAndBarPanel.add(new BarPanel(votingModel));

        this.setLayout(new BorderLayout());
        this.add(questionAndAddCandidatePanel, BorderLayout.NORTH);

        final JPanel centerPanel = new JPanel(new BorderLayout(10, 10));
        centerPanel.add(incrementAndSetPanel, BorderLayout.NORTH);
        centerPanel.add(cakeAndBarPanel, BorderLayout.CENTER);

        this.add(centerPanel, BorderLayout.CENTER);

        this.setSize(900, 600);
        this.setVisible(true);
    }

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente.
    public static void main(final String[] args)
    {
        if (args.length < 1)
        {
            throw new IllegalArgumentException("Es muss zumindest die Frage angegeben werden!");
        }

        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException | InstantiationException
        | IllegalAccessException | UnsupportedLookAndFeelException e)
        {
            e.printStackTrace();
        }

        final String question = args[0];
        final String[] candidates = Arrays.copyOfRange(args, 1, args.length);
        final IPoll votingModel = new VotingModel(question, candidates);

        new VotingFrame(question, votingModel);
    }
     */

    /**
     * Basisklasse für alle Panels, die auf dem {@link VotingFrame} angezeigt
     * werden.
     * 
     * @author Patrick Fries
     */
    public static class TitledPanel extends JPanel
    {
        /**
         * Initialisiert ein neues TitledPanel-Objekt mit den übergebenen
         * Argumenten.
         * 
         * @param title
         *            Titel, der in einem Rahmen um das Panel angezeigt werden
         *            soll.
         */
        public TitledPanel(final String title)
        {
            this.setBorder(BorderFactory.createTitledBorder(title));
        }
    }

    /**
     * Panel, auf dem eine Frage angezeigt wird.
     * 
     * @author Patrick Fries
     */
    private static class QuestionPanel extends TitledPanel
    {
        /**
         * Initialisiert ein neues QuestionPanel-Objekt mit den übergebenen
         * Argumenten.
         * 
         * @param question
         *            Anzuzeigende Frage.
         */
        public QuestionPanel(final String question)
        {
            super("Frage");
            this.setLayout(new BorderLayout());
            this.add(new JLabel(question, SwingConstants.CENTER));
        }
    }

    /**
     * Panel, auf dem ein {@link AddCandidateTextField}-Controller nebst einem
     * Beschreibungstext angezeigt wrid.
     * 
     * @author Patrick Fries
     */
    private static class AddCandidatePanel extends TitledPanel
    {
        /**
         * Initialisiert ein neues NewCandidatePanel-Objekt mit den übergebenen
         * Argumenten.
         * 
         * @param votingModel
         *            Modell, zu dem sich über den angezeigten
         *            {@link AddCandidateTextField} -Controller Kandidaten
         *            hinzufügen lassen sollen.
         */
        public AddCandidatePanel(final IPoll votingModel)
        {
            super("Neue Antwortmöglichkeit hinzufügen");

            this.setLayout(new GridLayout(1, 0, 10, 10));
            this.add(new JLabel("Zusätzliche Antwortmöglichkeit:", SwingConstants.RIGHT));
            this.add(new AddCandidateTextField(votingModel));
        }
    }

    /**
     * Panel, welches zu jedem in einem {@link IPoll} enthaltenen
     * Kandidaten eine Zeile bestehend aus {@link LabelView} und
     * {@link VoteIncrementButton} anzeigt.
     * 
     * @author Patrick Fries
     */
    private static class IncrementPanel extends TitledPanel implements IPollChangeListener
    {
        /**
         * Modell, auf das die {@link LabelView}s und
         * {@link VoteIncrementButton}s zugreifen.
         */
        private final IPoll votingModel;

        private String[] answers;

        /**
         * Initialisiert ein neues IncrementPanel-Objekt mit den übergebenen
         * Argumenten.
         * 
         * @param votingModel
         *            Modell, auf das die {@link LabelView}s und
         *            {@link VoteIncrementButton}s zugreifen sollen.
         */
        public IncrementPanel(final IPoll votingModel)
        {
            super("Erhöhen");

            this.votingModel = votingModel;
            this.votingModel.addPollChangeListener(this);
            this.answers = this.votingModel.getPollData().getAnswers();

            this.setLayout(new GridLayout(0, 2, 10, 6));
            for (int i = 0; i < this.answers.length; i++)
            {
                this.addComponentsForCandidateWithIndex(i);
            }
        }

        /**
         * Hilfsmethode, welche die grafischen Elemente für den Kandidaten mit
         * dem übergebenen Index zum Panel hinzufügen.
         * 
         * @param index
         *            Siehe Methodenbeschreibung.
         */
        private void addComponentsForCandidateWithIndex(final int index)
        {
            this.add(new LabelView(this.votingModel, index));
            this.add(new VoteIncrementButton(this.votingModel, index));
        }

        @Override
        public void voteChanged(PollData data)
        {
            // Funktionalität wird hier nicht benötigt.

        }

        @Override
        public void answerAdded(PollData data)
        {
            this.answers = data.getAnswers();
            this.addComponentsForCandidateWithIndex(this.answers.length - 1);

            // Neue Komponenten wurden zum Layout hinzugefuegt waehrend das
            // Layout bereits angezeigt
            // wurde. Da Swing ein einem solchen Fall nicht selbst das Layout
            // gerade biegt, muss man
            // das hier explizit anstossen.
            this.revalidate();
            
        }
    }

    /**
     * Panel, welches zu jedem in einem {@link IPoll} enthaltenen
     * Kandidaten eine Zeile bestehend aus einem {@link JLabel} mit dem Namen
     * des Kandidaten sowie einer {@link TextfieldView} anzeigt.
     * 
     * @author Patrick Fries
     */
    private static class SetPanel extends TitledPanel implements IPollChangeListener
    {
        /**
         * Modell, auf das die {@link LabelView}s und
         * {@link VoteIncrementButton}s zugreifen.
         */
        private final IPoll votingModel;

        private String[] answers;

        /**
         * Initialisiert ein neues IncrementPanel-Objekt mit den übergebenen
         * Argumenten.
         * 
         * @param votingModel
         *            Modell, auf das die {@link LabelView}s und
         *            {@link VoteIncrementButton}s zugreifen sollen.
         */
        public SetPanel(final IPoll votingModel)
        {
            super("Setzen");

            this.votingModel = votingModel;
            this.votingModel.addPollChangeListener(this);
            this.answers = this.votingModel.getPollData().getAnswers();

            this.setLayout(new GridLayout(0, 2, 10, 6));
            for (int i = 0; i < this.answers.length; i++)
            {
                this.addComponentsForCandidateWithIndex(i);
            }
        }

        /**
         * Hilfsmethode, welche die grafischen Elemente für den Kandidaten mit
         * dem übergebenen Index zum Panel hinzufügen.
         * 
         * @param index
         *            Siehe Methodenbeschreibung.
         */
        private void addComponentsForCandidateWithIndex(final int index)
        {
            this.add(new JLabel(this.answers[index], SwingConstants.RIGHT));

            final TextfieldView view = new TextfieldView(this.votingModel, index);
            view.addActionListener(new VoteSetActionListener(this.votingModel, index));
            this.add(view);
        }

        @Override
        public void voteChanged(PollData data)
        {
            // Funktionalität wird hier nicht benötigt.
        }

        @Override
        public void answerAdded(PollData data)
        {            
            this.answers = data.getAnswers();
            this.addComponentsForCandidateWithIndex(this.answers.length - 1);

            // Neue Komponenten wurden zum Layout hinzu gefügt während das
            // Layout bereits angezeigt
            // wurde. Da Swing ein einem solchen Fall nicht selbst das Layout
            // gerade biegt, muss man
            // das hier explizit anstoßen.
            this.revalidate();
        }
    }

    /**
     * Panel, welches die Stimmenverteilung eines {@link IPoll} mit einer
     * {@link CakeView} darstellt.
     * 
     * @author Patrick Fries
     */
    private static class CakePanel extends TitledPanel
    {
        /**
         * Initialisiert ein neues CakePanel-Objekt mit den übergebenen
         * Argumenten.
         * 
         * @param votingModel
         *            Modell, dessen Inhalte dargestellt werden sollen.
         */
        public CakePanel(final IPoll votingModel)
        {
            super("Tortendiagramm");
            this.setLayout(new BorderLayout());
            this.add(new CakeView(votingModel));
        }
    }

    /**
     * Panel, welches zu jedem in einem {@link IPoll} enthaltenen
     * Kandidaten eine {@link BarView} anzeigt.
     * 
     * @author Patrick Fries
     */
    private static class BarPanel extends TitledPanel implements IPollChangeListener
    {
        /**
         * Modell, auf das die {@link LabelView}s und
         * {@link VoteIncrementButton}s zugreifen.
         */
        private final IPoll votingModel;

        private String[] answers;
        /**
         * Initialisiert ein neues IncrementPanel-Objekt mit den übergebenen
         * Argumenten.
         * 
         * @param votingModel
         *            Modell, auf das die {@link LabelView}s und
         *            {@link VoteIncrementButton}s zugreifen sollen.
         */
        public BarPanel(final IPoll votingModel)
        {
            super("Balkendiagramm");

            this.votingModel = votingModel;
            this.votingModel.addPollChangeListener(this);
            this.answers = this.votingModel.getPollData().getAnswers();

            this.setLayout(new GridLayout(0, 1));
            for (int i = 0; i < this.answers.length; i++)
            {
                this.addComponentsForCandidateWithIndex(i);
            }
        }

        /**
         * Hilfsmethode, welche die grafischen Elemente für den Kandidaten mit
         * dem übergebenen Index zum Panel hinzufügen.
         * 
         * @param index
         *            Siehe Methodenbeschreibung.
         */
        private void addComponentsForCandidateWithIndex(final int index)
        {
            this.add(new BarView(this.votingModel, index));
        }

        @Override
        public void voteChanged(PollData data)
        {
            // Funktionalität wird hier nicht benötigt.
            
        }

        @Override
        public void answerAdded(PollData data)
        {
            this.answers = data.getAnswers();
            this.addComponentsForCandidateWithIndex(this.answers.length - 1);

            // Neue Komponenten wurden zum Layout hinzu gefügt während das
            // Layout bereits angezeigt
            // wurde. Da Swing ein einem solchen Fall nicht selbst das Layout
            // gerade biegt, muss man
            // das hier explizit anstoßen.
            this.revalidate();
        }
    }
}
