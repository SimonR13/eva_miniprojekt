package gui.mvc.voting.controller.voting;

import gui.mvc.voting.model.interfaces.IPoll;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * Controller für ein {@link IPoll} in Form eines {@link JButton}s,
 * welcher die Stimmen eines Kandidaten bei einem Klick um eins erhöht.
 * 
 * @author Patrick Fries
 */
@SuppressWarnings("serial")
public class VoteIncrementButton extends JButton implements ActionListener
{
    /**
     * Modell, das den Kandidaten enthält, dessen Stimmen inkrementiert werden
     * sollen.
     */
    private final IPoll votingModel;

    /** Indexposition des betreffenden Kandidaten aus dem Modell. */
    private final int candidateIndex;

    /**
     * Initialisiert ein neues VoteIncrementButton-Objekt mit den übergebenen
     * Argumenten.
     * 
     * @param votingModel
     *            Modell, das den Kandidaten enthält, dessen Stimmen
     *            inkrementiert werden sollen.
     * @param candidateIndex
     *            Indexposition des betreffenden Kandidaten aus dem Modell.
     */
    public VoteIncrementButton(final IPoll votingModel, final int candidateIndex)
    {
        super("Erhöhen");

        this.votingModel = votingModel;
        this.candidateIndex = candidateIndex;

        this.addActionListener(this);
    }

    @Override
    public void actionPerformed(final ActionEvent e)
    {
        this.votingModel.incrementVotes(this.candidateIndex);
    }
}
