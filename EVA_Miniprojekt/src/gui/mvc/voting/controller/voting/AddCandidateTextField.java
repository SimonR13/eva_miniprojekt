package gui.mvc.voting.controller.voting;

import gui.mvc.voting.model.interfaces.IPoll;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

/**
 * Controller für ein {@link IPoll} in Form eines {@link JTextField}s,
 * welcher neue Kandidaten in das Modell einfügen kann.
 * 
 * @author Patrick Fries
 */
@SuppressWarnings("serial")
public class AddCandidateTextField extends JTextField implements ActionListener
{
    /** Modell, in das neue Kandidat eingefügt werden sollen. */
    private final IPoll votingModel;

    /**
     * Initialisiert ein neues NewCandidateTextField-Objekt mit den übergebenen
     * Argumenten.
     * 
     * @param votingModel
     *            Modell, in das neue Kandidat eingefügt werden sollen.
     */
    public AddCandidateTextField(final IPoll votingModel)
    {
        this.votingModel = votingModel;
        this.addActionListener(this);
    }

    @Override
    public void actionPerformed(final ActionEvent e)
    {
        this.votingModel.addAnswer(this.getText());
        this.setText("");
    }
}
