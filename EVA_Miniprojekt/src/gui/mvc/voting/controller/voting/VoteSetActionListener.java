package gui.mvc.voting.controller.voting;

import gui.mvc.voting.model.interfaces.IPoll;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.text.JTextComponent;

/**
 * Controller für ein {@link IPoll} in Form eines {@link ActionListener}
 * s, welcher an einer {@link JTextComponent} angemeldet werden kann und bei
 * Benachrichtigung die Stimmen eines Kandidaten auf dem innerhalb der
 * {@link JTextComponent} dargestellten Wert setzt.
 * 
 * @author Patrick Fries
 */
public class VoteSetActionListener implements ActionListener
{
    /**
     * Modell, das den Kandidaten enthält, dessen Stimmen gesetzt werden sollen.
     */
    private final IPoll votingModel;

    /** Indexposition des betreffenden Kandidaten aus dem Modell. */
    private final int candidateIndex;

    /**
     * Initialisiert ein neues VoteSetActionListener-Objekt mit den übergebenen
     * Argumenten.
     * 
     * @param votingModel
     *            Modell, das den Kandidaten enthält, dessen Stimmen gesetzt
     *            werden sollen.
     * @param candidateIndex
     *            Indexposition des betreffenden Kandidaten aus dem Modell.
     */
    public VoteSetActionListener(final IPoll votingModel, final int candidateIndex)
    {
        this.votingModel = votingModel;
        this.candidateIndex = candidateIndex;
    }

    @Override
    public void actionPerformed(final ActionEvent e)
    {
        final Object source = e.getSource();
        if (source instanceof JTextComponent)
        {
            final String votesString = ((JTextComponent) source).getText();
            final int votes = Integer.valueOf(votesString);
            this.votingModel.setVotes(this.candidateIndex, votes);

        }
        else
        {
            throw new RuntimeException("Dieser ActionListener kann nur mit " + "einer JTextComponent zusammenarbeiten");
        }
    }
}
