package gui.mvc.voting.controller.survey;

import gui.mvc.voting.model.interfaces.IAllPolls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class AddSurveyButton extends JButton implements ActionListener
{

    private final IAllPolls surveyModel;

    private final JTextField txtFieldId;

    private final JTextField txtFieldQuestion;

    public AddSurveyButton(final IAllPolls surveyModel, final JTextField txtFieldId, final JTextField txtFieldQuestion)
    {
        super("Anlegen");
        this.surveyModel = surveyModel;
        this.txtFieldId = txtFieldId;
        this.txtFieldQuestion = txtFieldQuestion;

        this.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        final String id = this.txtFieldId.getText();
        final String question = this.txtFieldQuestion.getText();

        this.txtFieldId.setText("");
        this.txtFieldQuestion.setText("");
        
        this.surveyModel.addPoll(id, question);
    }

}
