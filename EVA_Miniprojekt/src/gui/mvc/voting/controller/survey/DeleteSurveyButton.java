package gui.mvc.voting.controller.survey;

import gui.mvc.voting.model.interfaces.IAllPolls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class DeleteSurveyButton extends JButton implements ActionListener
{
    private final IAllPolls surveyModel;

    private final String id;

    public DeleteSurveyButton(final IAllPolls surveyModel, final String id)
    {
        super("Löschen");
        this.surveyModel = surveyModel;
        this.id = id;

        this.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        this.surveyModel.removePoll(id);
    }

}
