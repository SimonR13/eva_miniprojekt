package gui.mvc.voting.controller.survey;

import gui.mvc.voting.VotingFrame;
import gui.mvc.voting.model.interfaces.IAllPolls;
import gui.mvc.voting.model.interfaces.IPoll;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class ShowSurveyButton extends JButton implements ActionListener
{
    private final IAllPolls surveyModel;

    private final String id;

    public ShowSurveyButton(final IAllPolls surveyModel, final String id)
    {
        super("Ansehen");
        this.surveyModel = surveyModel;
        this.id = id;
        
        this.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        final IPoll votingModel = this.surveyModel.getPoll(this.id);
        new VotingFrame(votingModel.getQuestion(), votingModel);
    }

}
