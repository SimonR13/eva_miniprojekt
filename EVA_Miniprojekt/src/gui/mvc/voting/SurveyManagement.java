package gui.mvc.voting;

import gui.mvc.voting.VotingFrame.TitledPanel;
import gui.mvc.voting.controller.survey.AddSurveyButton;
import gui.mvc.voting.controller.survey.DeleteSurveyButton;
import gui.mvc.voting.controller.survey.ShowSurveyButton;
import gui.mvc.voting.model.SurveyModel;
import gui.mvc.voting.model.interfaces.IAllPolls;
import gui.mvc.voting.model.interfaces.IAllPollsChangeListener;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

@SuppressWarnings("serial")
public class SurveyManagement extends JFrame
{

    public SurveyManagement(final IAllPolls surveyModel)
    {
        super("Umfragenverwaltung");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        final JPanel questionsPanel = new JPanel(new GridLayout(1, 0, 10, 10));
        questionsPanel.add(new QuestionsPanel(surveyModel));

        final JPanel addQuestionPanel = new JPanel(new GridLayout(1, 0, 10, 10));
        addQuestionPanel.add(new AddQuestionPanel(surveyModel));

        this.setLayout(new BorderLayout());

        final JPanel centerPanel = new JPanel(new BorderLayout(10, 10));
        centerPanel.add(questionsPanel, BorderLayout.NORTH);
        centerPanel.add(addQuestionPanel, BorderLayout.SOUTH);

        this.add(centerPanel, BorderLayout.CENTER);

        this.setSize(900, 600);
        this.setVisible(true);
    }

    private static class QuestionsPanel extends TitledPanel implements IAllPollsChangeListener
    {

        private final IAllPolls surveyModel;

        public QuestionsPanel(final IAllPolls surveyModel)
        {
            super("Überblick aller Umfragen");

            this.surveyModel = surveyModel;
            this.surveyModel.addAllPollsChangeListener(this);

            this.setLayout(new GridLayout(this.surveyModel.getAllIdsAndQuestions().length, 3));
            addQuestions();
        }

        private void addQuestions()
        {
            String[][] idsAndQuestions = this.surveyModel.getAllIdsAndQuestions();
            if (idsAndQuestions.length > 0)
            {
                for (int i = 0; i < idsAndQuestions.length; i++)
                {
                    String id = idsAndQuestions[i][0];
                    String question = idsAndQuestions[i][1];
                    
                    this.add(new JLabel(question));
                    this.add(new ShowSurveyButton(this.surveyModel, id));
                    this.add(new DeleteSurveyButton(this.surveyModel, id));
                }
            }
        }

        @Override
        public void pollAdded(String id, String question)
        {
            this.setLayout(new GridLayout(this.surveyModel.getAllIdsAndQuestions().length, 3));
            
            this.add(new JLabel(question));
            this.add(new ShowSurveyButton(this.surveyModel, id));
            this.add(new DeleteSurveyButton(this.surveyModel, id));
            this.revalidate();
        }

        @Override
        public void pollRemoved(String id)
        {
            this.removeAll();
            this.setLayout(new GridLayout(this.surveyModel.getAllIdsAndQuestions().length, 3));
            
            String[][] idsAndQuestions = this.surveyModel.getAllIdsAndQuestions();
            if (idsAndQuestions.length > 0)
            {
                for (int i = 0; i < idsAndQuestions.length; i++)
                {
                    addQuestions();
                }
            }
            
            this.revalidate();
        }
    }

    private static class AddQuestionPanel extends TitledPanel
    {

        public AddQuestionPanel(final IAllPolls surveyModel)
        {
            super("Neue Umfrage anlegen");

            final JTextField txtFieldId = new JTextField();
            final JTextField txtFieldQuestion = new JTextField();

            this.setLayout(new GridLayout(1, 0, 10, 10));
            this.add(new JLabel("Kennung:", SwingConstants.RIGHT));
            this.add(txtFieldId);
            this.add(new JLabel("Frage:", SwingConstants.RIGHT));
            this.add(txtFieldQuestion);
            this.add(new AddSurveyButton(surveyModel, txtFieldId, txtFieldQuestion));
        }

    }

    public static void main(String[] args)
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException | InstantiationException
        | IllegalAccessException | UnsupportedLookAndFeelException e)
        {
            e.printStackTrace();
        }

        if (args.length > 1)
        {
            final String kennung = args[0];
            final String question = args[1];
            final IAllPolls surveyModel = new SurveyModel(kennung, question);

            new SurveyManagement(surveyModel);
        }
        else
        {
            new SurveyManagement(new SurveyModel());
        }

    }

}
