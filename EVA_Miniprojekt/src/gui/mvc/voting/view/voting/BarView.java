package gui.mvc.voting.view.voting;

import gui.mvc.voting.model.PollData;
import gui.mvc.voting.model.interfaces.IPoll;
import gui.mvc.voting.model.interfaces.IPollChangeListener;
import gui.mvc.voting.view.widget.AAEnablingJPanel;

import java.awt.Graphics;

/**
 * View für ein {@link IPoll}, welche die prozentualen Stimmenanteile
 * eines Kandidaten in Form eines Balkens darstellt.
 * 
 * @author Patrick Fries
 */
@SuppressWarnings("serial")
public class BarView extends AAEnablingJPanel implements IPollChangeListener
{
    /** Abstand, den die Balken zu den Rändern mindestens einhalten sollen. */
    public static final int BORDER_WIDTH = 4;

    /**
     * Maximale Breite, die für die Darstellung des Kandidatennamens verwendet
     * werden soll.
     */
    public static final int TEXT_WIDTH = 120;

    /** Modell, dessen Inhalte dargestellt werden. */
    private final IPoll votingModel;
    
    private String[] answers;

    /** Indexposition des betreffenden Kandidaten aus dem Modell. */
    private final int candidateIndex;

    /**
     * Initialisiert ein neues BarView-Objekt mit den übergebenen Argumenten.
     * 
     * @param votingModel
     *            Modell, aus dem die Stimmanteile eines Kandidaten angezeigt
     *            werden.
     * @param candidateIndex
     *            Indexposition des betreffenden Kandidaten aus dem Modell.
     */
    public BarView(final IPoll votingModel, final int candidateIndex)
    {
        this.votingModel = votingModel;
        this.candidateIndex = candidateIndex;

        this.answers = this.votingModel.getPollData().getAnswers();
        
        this.votingModel.addPollChangeListener(this);
    }

    @Override
    protected void paintComponent(final Graphics g)
    {
        super.paintComponent(g);
        final int centerY = this.getHeight() / 2;

        // Kandidatennamen zeichnen
        final int nameX = BarView.BORDER_WIDTH;
        final int nameY = centerY + (g.getFontMetrics().getAscent() / 2);
        g.drawString(this.answers[this.candidateIndex], nameX, nameY);

        // Balken zeichnen
        final int barX = nameX + BarView.TEXT_WIDTH;
        final int barY = BarView.BORDER_WIDTH;
        final int barHeight = this.getHeight() - (2 * BarView.BORDER_WIDTH);
        final int barWidthMax = this.getWidth() - barX - BarView.BORDER_WIDTH;
        final int filledBarWidth = (barWidthMax * this.votingModel.getPollData().getAnswerPercentage(this.candidateIndex)) / 100;
        g.fillRect(barX, barY, filledBarWidth, barHeight);
        g.drawRect(barX, barY, barWidthMax, barHeight);
    }

    @Override
    public void voteChanged(PollData data)
    {
        this.repaint();
    }

    @Override
    public void answerAdded(PollData data)
    {
        // Funktionalität wird hier nicht benötigt.
    }
}
