package gui.mvc.voting.view.voting;

import gui.mvc.voting.model.PollData;
import gui.mvc.voting.model.interfaces.IPoll;
import gui.mvc.voting.model.interfaces.IPollChangeListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * View für ein {@link IPoll} in Form eines {@link JLabel}s, welches die
 * Daten eines durch das Modell repräsentierten Kandidaten in der Form
 * "[Name]: [Stimmen] von [Gesamtstimmen] ([Prozentuale Stimmen]%)" anzeigt
 * (Siehe {@link LabelView#FORMAT_STRING}).
 * 
 * @author Patrick Fries
 */
@SuppressWarnings("serial")
public class LabelView extends JLabel implements IPollChangeListener
{
    /** Formatvorlage für die angezeigte Zeichenkette. */
    public static final String FORMAT_STRING = "%s: %d von %d (%d%s)";

    /** Modell, aus dem die Daten eines Kandidaten angezeigt werden. */
    private final IPoll votingModel;

    /** Indexposition des betreffenden Kandidaten aus dem Modell. */
    private final int candidateIndex;

    /**
     * Initialisiert ein neues LabelView-Objekt mit den übergebenen Argumenten.
     * 
     * @param votingModel
     *            Modell, aus dem die Daten eines Kandidaten angezeigt werden.
     * @param candidateIndex
     *            Indexposition des betreffenden Kandidaten aus dem Modell.
     */
    public LabelView(final IPoll votingModel, final int candidateIndex)
    {
        this.votingModel = votingModel;
        this.candidateIndex = candidateIndex;

        this.setHorizontalAlignment(SwingConstants.RIGHT);
        this.votingModel.addPollChangeListener(this);

        this.voteChanged(votingModel.getPollData());
    }

    @Override
    public void voteChanged(PollData data)
    {
        final String newText = String.format(LabelView.FORMAT_STRING, 
            data.getAnswers()[this.candidateIndex], 
            data.getVotes()[(this.candidateIndex)], 
            data.getTotalVotes(), 
            data.getAnswerPercentage(this.candidateIndex), '%');
        this.setText(newText);

    }

    @Override
    public void answerAdded(PollData data)
    {
        // Funktionalität wird hier nicht benötigt.
    }
}
