package gui.mvc.voting.view.voting;

import gui.mvc.voting.model.PollData;
import gui.mvc.voting.model.interfaces.IPoll;
import gui.mvc.voting.model.interfaces.IPollChangeListener;

import javax.swing.JTextField;

/**
 * View für ein {@link IPoll} in Form eines {@link JTextField}s, welches
 * die Stimmen eines durch das Modell repräsentierten Kandidaten anzeigt.
 * 
 * @author Patrick Fries
 */
@SuppressWarnings("serial")
public class TextfieldView extends JTextField implements IPollChangeListener
{
    /** Modell, aus dem die Stimmen eines Kandidaten angezeigt werden. */
    private final IPoll votingModel;

    /** Indexposition des betreffenden Kandidaten aus dem Modell. */
    private final int candidateIndex;

    /**
     * Initialisiert ein neues TextfieldView-Objekt mit den übergebenen
     * Argumenten.
     * 
     * @param votingModel
     *            Modell, aus dem die Stimmen eines Kandidaten angezeigt werden.
     * @param candidateIndex
     *            Indexposition des betreffenden Kandidaten aus dem Modell.
     */
    public TextfieldView(final IPoll votingModel, final int candidateIndex)
    {
        this.votingModel = votingModel;
        this.candidateIndex = candidateIndex;

        this.votingModel.addPollChangeListener(this);
        this.voteChanged(this.votingModel.getPollData());
    }

    @Override
    public void voteChanged(PollData data)
    {
        this.setText(String.valueOf(data.getVotes()[this.candidateIndex]));

    }

    @Override
    public void answerAdded(PollData data)
    {
        // nicht benoetigt

    }
}
