package gui.mvc.voting.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gui.mvc.voting.model.interfaces.IAllPolls;
import gui.mvc.voting.model.interfaces.IAllPollsChangeListener;
import gui.mvc.voting.model.interfaces.IPoll;

public class SurveyModel implements IAllPolls
{

    private final Map<String, IPoll> surveys = new HashMap<String, IPoll>();

    private final List<IAllPollsChangeListener> allPollChangeListener;

    public SurveyModel()
    {
        this.allPollChangeListener = new ArrayList<IAllPollsChangeListener>();
    }

    public SurveyModel(final String kennung, final String question)
    {
        this();
        this.addPoll(kennung, question);
    }

    @Override
    public IPoll addPoll(String id, String question)
    {
        return addPoll(id, question, new String[]
        {});
    }

    @Override
    public IPoll addPoll(String id, String question, String[] answers)
    {
        final IPoll poll = new VotingModel(question, answers);
        this.surveys.put(id, poll);
        this.allPollsChangeListenerAdd(id, question);
        return poll;
    }

    @Override
    public void removePoll(String id)
    {
        this.surveys.remove(id);
        this.allPollsChangeListenerRemove(id);
    }

    @Override
    public IPoll getPoll(String id)
    {
        return this.surveys.get(id);
    }

    @Override
    public String[][] getAllIdsAndQuestions()
    {
        final String[][] retVal = new String[this.surveys.size()][2];

        final Set<String> keySet = this.surveys.keySet();
        int i = 0;

        for (String key : keySet)
        {
            IPoll tmp = this.surveys.get(key);
            retVal[i][0] = key;
            retVal[i][1] = tmp.getQuestion();
        }

        return retVal;
    }

    @Override
    public void addAllPollsChangeListener(IAllPollsChangeListener l)
    {
        this.allPollChangeListener.add(l);
    }

    @Override
    public void removeAllPollsChangeListener(IAllPollsChangeListener l)
    {
        this.allPollChangeListener.remove(l);
    }

    public List<IAllPollsChangeListener> getAllPollsChangeListener()
    {
        return this.allPollChangeListener;
    }

    private void allPollsChangeListenerAdd(final String id, final String question)
    {
        for (IAllPollsChangeListener listener : this.allPollChangeListener)
        {
            listener.pollAdded(id, question);
        }
    }

    private void allPollsChangeListenerRemove(String id)
    {
        for (IAllPollsChangeListener listener : this.allPollChangeListener)
        {
            listener.pollRemoved(id);
        }
    }
}
