package gui.mvc.voting.model;

import gui.mvc.voting.model.interfaces.IPoll;
import gui.mvc.voting.model.interfaces.IPollChangeListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Modellkomponente, die eine Menge von Zuordnungen zwischen Kandidatennamen und
 * Stimmen, die für die betreffenden Kandidaten abgegeben wurden, verwaltet.
 * 
 * @author Patrick Fries
 */
public class VotingModel implements IPoll
{

    /** Frage */
    private final String question;

    /** Liste der verwalteten Kandidaten mit Stimmen. */
    private final ArrayList<Candidate> answers;

    /** Anzahl der insgesamt abgegebenen Stimmen. */
    private int totalVotes;

    /** Liste der Listener, die bei Änderungen benachrichtigt werden. */
    /** private final List<IVotingModelListener> votingModelListeners; */

    /** Liste der Listener, die bei Änderungen benachrichtigt werden. */
    private final List<IPollChangeListener> pollChangeListeners;

    /**
     * Initialisiert ein neues VotingModel-Objekt mit den übergebenen
     * Argumenten.
     * 
     * @param answers
     *            Namen der Kandidaten, die initial im Modell vorhanden sein
     *            sollen.
     */
    public VotingModel(final String quest, final String[] answers)
    {
        this.question = quest;
        this.answers = new ArrayList<Candidate>();
        for (final String currentAnswer : answers)
        {
            this.answers.add(new Candidate(currentAnswer));
        }

        this.pollChangeListeners = new ArrayList<IPollChangeListener>();
    }

    public void addCandidate(final String name)
    {
        this.answers.add(new Candidate(name));
        final List<IPollChangeListener> copyOfListenerList = new ArrayList<IPollChangeListener>(this.pollChangeListeners);
        for (final IPollChangeListener currentListener : copyOfListenerList)
        {
            currentListener.answerAdded(getPollData());
        }
    }

    public String getCandidateName(final int index)
    {
        return this.answers.get(index).getName();
    }

    public int getCandidateVotes(final int index)
    {
        return this.answers.get(index).getVotes();
    }

    public int getTotalVotes()
    {
        return this.totalVotes;
    }

    public int getCandidatePercentage(final int index)
    {
        final double candidateVotes = this.getCandidateVotes(index);
        final double percentage = candidateVotes / this.getTotalVotes() * 100;
        return (int) Math.round(percentage);
    }

    public int getNumberOfCandidates()
    {
        return this.answers.size();
    }

    private void incrementCandidateVotes(final int index)
    {
        this.answers.get(index).incrementVotes();
        this.totalVotes++;

        this.fireVotesChanged();
    }

    private void setCandidateVotes(final int index, final int votes)
    {
        final Candidate candidate = this.answers.get(index);
        final int oldVotes = candidate.getVotes();
        candidate.setVotes(votes);

        final int difference = votes - oldVotes;
        this.totalVotes += difference;

        this.fireVotesChanged();
    }

    /*
     * public void addVotingModelListener(final IVotingModelListener l) {
     * this.votingModelListeners.add(l); }
     * 
     * public void removeVotingModelListener(final IVotingModelListener l) {
     * this.votingModelListeners.remove(l); }
     */

    /**
     * Hilfsmethode, die alle angemeldeten {@link IVotingModelListener} über
     * eine Änderung bei der Stimmenverteilung informiert.
     **/

    private void fireVotesChanged()
    {
        for (final IPollChangeListener currentListener : this.pollChangeListeners)
        {
            currentListener.voteChanged(getPollData());
        }
    }

    /**
     * Klasse zur Repräsentationen eines Kandidaten mit seinen Simmen.
     * 
     * @author Patrick Fries
     */
    private static class Candidate
    {
        /** Name des Kandidaten. */
        private final String name;

        /** Stimmen, die für den Kandidaten abgegeben wurden. */
        private int votes;

        /**
         * Initialisiert ein neues Candidate-Objekt mit den übergebenen
         * Argumenten.
         * 
         * @param name
         *            Name des Kandidaten.
         */
        public Candidate(final String name)
        {
            this.name = name;
        }

        /**
         * Liefert den Namen des Kandidaten zurück.
         * 
         * @return Name des Kandidaten.
         */
        public String getName()
        {
            return this.name;
        }

        /**
         * Liefert die Stimmen zurück, die für den Kandidaten abgegeben wurden.
         * 
         * @return Stimmen, die für den Kandidaten abgegeben wurden.
         */
        public int getVotes()
        {
            return this.votes;
        }

        /**
         * Erhöht die Simmen, die für den Kandidaten abgegeben wurden, um eins.
         */
        public void incrementVotes()
        {
            this.votes++;
        }

        /**
         * Setzt die Simmen, die für den Kandidaten abgegeben wurden.
         * 
         * @param votes
         *            Neue Anzahl an Stimmen.
         */
        public void setVotes(final int votes)
        {
            this.votes = votes;
        }
    }

    @Override
    public String getQuestion()
    {
        return this.question;
    }

    @Override
    public PollData getPollData()
    {
        final String[] ans = new String[this.answers.size()];
        final int[] votes = new int[this.answers.size()];
        for (int i = 0; i < this.answers.size(); i++)
        {
            final Candidate cand = this.answers.get(i);
            ans[i] = cand.getName();
            votes[i] = cand.getVotes();
        }
        PollData retVal = new PollData(getQuestion(), ans, votes, this.totalVotes);
        return retVal;
    }

    @Override
    public void addAnswer(String answer)
    {
        addCandidate(answer);
    }

    @Override
    public void setVotes(int answerIndex, int votes)
    {
        setCandidateVotes(answerIndex, votes);
    }

    @Override
    public void incrementVotes(int answerIndex)
    {
        incrementCandidateVotes(answerIndex);
    }

    @Override
    public void addPollChangeListener(IPollChangeListener pcl)
    {
        this.pollChangeListeners.add(pcl);
    }

    @Override
    public void removePollChangeListener(IPollChangeListener pcl)
    {
        this.pollChangeListeners.remove(pcl);
    }

    public List<IPollChangeListener> getPollChangeListeners()
    {
        return pollChangeListeners;
    }

}
