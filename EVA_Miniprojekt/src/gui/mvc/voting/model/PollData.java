package gui.mvc.voting.model;

/**
 * Wert-Objekt für die umfragerelevanten Inhalte einer {@link Poll}
 * implementierenden Modellkomponente.
 */
public class PollData
{
    /** Frage, die Thema der Umfrage ist. */
    private final String question;

    /** Antwortmöglichkeiten der Umfrage. */
    private final String[] answers;

    /** Stimmen, die auf die Antwortmöglichkeiten verteilt wurden. */
    private final int[] votes;

    /** Gesamtanzahl der bereits verteilten Stimmen. */
    private final int totalVotes;

    /**
     * Initialisiert ein neues {@link PollData}-Objekt mit den übergebenen
     * Argumenten.
     * 
     * @param question
     *            Frage, die Thema der Umfrage ist.
     * @param answers
     *            Antwortmöglichkeiten der Umfrage.
     * @param votes
     *            Stimmen, die auf die Antwortmöglichkeiten verteilt wurden.
     * @param totalVotes
     *            Gesamtanzahl der bereits verteilten Stimmen.
     */
    public PollData(final String question, final String[] answers, final int[] votes, final int totalVotes)
    {
        this.question = question;
        this.answers = answers;
        this.votes = votes;
        this.totalVotes = totalVotes;
    }

    /* Getter */
    public String getQuestion()
    {
        return question;
    }

    public String[] getAnswers()
    {
        return answers;
    }

    public int[] getVotes()
    {
        return votes;
    }

    public int getTotalVotes()
    {
        return totalVotes;
    }
    
    public int getAnswerPercentage(final int index)
    {
        final double candidateVotes = this.getVotes()[index];
        final double percentage = candidateVotes / this.getTotalVotes() * 100;
        return (int) Math.round(percentage);
    }
}
