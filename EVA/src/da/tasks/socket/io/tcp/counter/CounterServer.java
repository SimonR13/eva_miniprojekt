package da.tasks.socket.io.tcp.counter;

import java.io.IOException;
import java.net.ServerSocket;

import da.lecture.socket.io.tcp.TCPSocket;

/**
 * TCP-Server, der einen int-Zähler verwaltet und die Befehle "increment" und
 * "reset" versteht. Auf jeden Befehl wird der Zählerstand nach dem Befehl
 * geantwortet.
 */
public class CounterServer implements AutoCloseable
{
    /** Port, auf dem der TCP-Server auf Befehle lauscht. */
    public static final int DEFAULT_PORT = 1250;

    /** Verwalteter int-Zähler. */
    private int counter = 0;

    /** {@link ServerSocket}, auf dem Clientverbindungen angenommen werden. */
    private final ServerSocket sSocket;

    /**
     * Initialisiert eine neue CounterServer-Instanz mit den übergebenen
     * Argumenten.
     * 
     * @param port
     *            Port, auf dem ein {@link ServerSocket} zur Annahme von
     *            Clientverbindungen geöffnet werden soll.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim erstellen des
     *             {@link ServerSocket}s zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     */
    public CounterServer(final int serverPort) throws IOException
    {
        this(new ServerSocket(serverPort));
    }

    /**
     * Initialisiert eine neue CounterServer-Instanz mit den übergebenen
     * Argumenten.
     * 
     * @param sSocket
     *            {@link ServerSocket}, auf dem Clientverbindungen angenommen
     *            werden sollen.
     */
    public CounterServer(final ServerSocket sSocket)
    {
        this.sSocket = sSocket;
    }

    /**
     * Hauptprogramm. Als Kommandozeilenargument muss die Portnummer des Ports
     * angegeben werden, auf dem der Server sein {@link ServerSocket} öffnet.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Erzeugung des
     *             {@link ServerSocket}s zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     */
    public static void main(final String[] args)
    {
        int desiredServerPort = CounterServer.DEFAULT_PORT;

        if (args.length > 0)
        {
            try
            {
                desiredServerPort = Integer.parseInt(args[0]);
            }
            catch (NumberFormatException nfe)
            {
                System.err.println("Fehler beim parsen des Port! Weiter mit default-port...");
            }
        }

        try (final CounterServer cServ = new CounterServer(desiredServerPort))
        {
            cServ.runServer();
        }
        catch (Exception e)
        {
            System.err.println("Erstellen einer neuen CounterServer-Instanz fehlgeschlagen!");
            e.printStackTrace();
        }

    }

    /**
     * Methode aus der Schnittstelle {@link AutoCloseable} zum automatisierten
     * Schließen aller Ressourcen dieses {@link CounterServer}-Objektes.
     * 
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Schließen der Ressourcen zu
     *             einem Ein-/Ausgabefehler gekommen ist.
     */
    @Override
    public void close() throws IOException
    {
        this.sSocket.close();
    }

    /**
     * Nimmt wiederholt Anfragen von Clients entgegen, verarbeitet diese und
     * antwortet dem anfragenden Client.
     */
    public void runServer()
    {

        while (true)
        {
            try (final TCPSocket tcpSocket = new TCPSocket(sSocket.accept()))
            {
                String request = null;
                while ((request = tcpSocket.receiveLine()) != null)
                { // Blockiert bis Empfang
                    final String answer = processRequestAndReturnAnswer(request);
                    tcpSocket.sendLine(answer);
                }
            } // Schließen von tcpSocket automatisch durch
              // try-with-resources
            catch (IOException e)
            {
                System.err.println("Fehler beim Erstellen des TCPSockets! Weiter...");
                e.printStackTrace();
            }
        }
    }

    /**
     * Hilfsmethode, die einen übergebenen Befehl ausführt und den neuen
     * Zählerstand als Antwort zurückliefert.
     * 
     * @param request
     *            Auszuführender Befehl.
     * @return Zählerstand nach Ausführung des Befehls.
     */
    private String processRequestAndReturnAnswer(final String request)
    {
        if (request.equals("increment"))
        {
            counter++;
        }
        else if (request.equals("reset"))
        {
            counter = 0;
        }
        else if (request.equals("decrement"))
        {
            counter--;
        }
        else if (request.startsWith("set"))
        {
            final String[] req = request.split(" ");

            try
            {
                final int number = Integer.parseInt(req[1]);
                counter = number;
            }
            catch (NumberFormatException nfe)
            {
                System.err.println("Fehlerhafte Eingabe bei set X fuer X! Weiter...");
            }
        }

        return String.valueOf(counter);
    }

}
