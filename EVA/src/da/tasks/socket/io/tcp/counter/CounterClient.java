package da.tasks.socket.io.tcp.counter;

import java.io.IOException;

import da.lecture.socket.io.tcp.TCPSocket;

/**
 * TCP-Client, der verschiedene Befehle an einen {@link CounterServer} schickt.
 */
public class CounterClient implements AutoCloseable
{
    // Gegebenenfalls fehlen noch Attribute...

    private final TCPSocket tcpSocket;

    /**
     * Initialisiert eine neue CounterClient-Instanz mit den übergebenen
     * Argumenten.
     * 
     * @param serverAddress
     *            Adresse des Servers, auf dem die
     * 
     *            {@link CounterServer}-Anwendung läuft.
     * @param serverPort
     *            Port, auf dem die {@link CounterServer}-Anwendung
     * 
     *            auf eingehende Verbindungsanfragen lauscht.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Verbindungsaufbau mit
     * 
     *             dem {@link CounterServer} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public CounterClient(final String serverAddress, final int serverPort) throws IOException
    {
        this.tcpSocket = new TCPSocket(serverAddress, serverPort);
    }

    /**
     * Startet den Client. Es werden zuerst ein reset-Befehl und danach
     * <code>incrementCount</code> viele increment-Befehle gesendet. Nach jedem
     * versendeten Befehl wird die Antwort des Servers abgewartet und auf der
     * Konsole ausgegeben.
     * 
     * @param incrementCount
     *            Anzahl an zu versendenden increment-Befehlen.
     */
    public void runClient(final int incrementCount)
    {
        try
        {
            tcpSocket.sendLine("reset");
            System.out.println("Antwort: " + tcpSocket.receiveLine());

            for (int i = 0; i < incrementCount; i++)
            {
                tcpSocket.sendLine("increment");
                System.out.println("Antwort: " + tcpSocket.receiveLine());
            }
        }
        catch (IOException e)
        {
            System.err.println("IOException beim Senden der Befehle...");
            e.printStackTrace();
        }
    }

    /**
     * Methode aus der Schnittstelle {@link AutoCloseable} zum automatisierten
     * Schließen aller Ressourcen dieses {@link CounterClient}-Objektes.
     * 
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Schließen der
     * 
     *             Ressourcen zu einem Ein-/Ausgabefehler gekommen ist.
     */
    @Override
    public void close() throws IOException
    {
        this.tcpSocket.close();
    }

    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen Adresse und Port des
     * {@link CounterServer}s übergeben werden sowie die Anzahl an zu
     * versendenden 'increment'-Befehlen.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Verbindungsaufbau mit
     * 
     *             dem {@link CounterServer} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        String svrAdr = "localhost";
        int svrPort = 1250;
        int incrementCount = 1000;

        try
        {
            svrAdr = args[0];
            svrPort = Integer.parseInt(args[1]);
            incrementCount = Integer.parseInt(args[2]);
        }
        catch (NumberFormatException nfe)
        {
            System.err.println("Bitte n&auml;chstes mal Kommandozeilenargumente &uuml;bergeben! Weiter mit Default...");
        }
        catch (ArrayIndexOutOfBoundsException aobe)
        {
            System.err.println("Bitte n&auml;chstes mal Kommandozeilenargumente &uuml;bergeben! Weiter mit Default...");
        }

        try (final CounterClient cClient = new CounterClient(svrAdr, svrPort))
        {
            cClient.runClient(incrementCount);
        }
    }

}