package da.tasks.socket.io.tcp.relay;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/** TCP-Relay zur Weiterleitung der Daten einer TCP-Verbindung. */
public class TCPRelay
{
    /** {@link ServerSocket} zur Annahme von Clientverbindungen. */
    private final ServerSocket sSocket;

    /** Adresse und Port, an die weitergeleitet wird. */
    private final InetSocketAddress addressToRelayTo;

    /**
     * Initialisiert eine neue TCPRelay-Instanz mit den übergebenen Argumenten.
     * 
     * @param sSocket
     *            {@link ServerSocket}, das zur Annahme von Clientverbindungen
     *            genutzt werden soll.
     * @param addressToRelayTo
     *            Adresse und Port, an die weitergeleitet werden soll.
     */
    public TCPRelay(final ServerSocket sSocket, final InetSocketAddress addressToRelayTo)
    {
        this.sSocket = sSocket;
        this.addressToRelayTo = addressToRelayTo;
    }

    public static void relayThread(final Runnable runa)
    {
        new Thread(runa).start();
    }

    /**
     * Startet das TCP-Relay.
     * 
     * @throws IOException
     * @throws UnknownHostException
     */
    public void startRelaying() throws IOException
    {
        while (true)
        {
            final Socket clientSocket = sSocket.accept();
            final Socket targetSocket = new Socket(addressToRelayTo.getAddress(), addressToRelayTo.getPort());
            InputStream clientInputStream = clientSocket.getInputStream();
            OutputStream clientOutputStream = clientSocket.getOutputStream();
            InputStream targetInputStream = targetSocket.getInputStream();
            OutputStream targetOutputStream = targetSocket.getOutputStream();

            System.out.println("Incoming Contact... source: " + clientSocket + "; target: " + targetSocket);
            TCPRelay.relayThread(() -> {
                byte[] buf = null;

                try
                {
                    buf = new byte[targetSocket.getReceiveBufferSize()];
                }
                catch (SocketException se)
                {
                    System.err.println(se.getMessage());
                }

                try
                {
                    int r = 1;
                    while (r > 0)
                    {
                        r = targetInputStream.read(buf);
                        if (r > 0)
                            clientOutputStream.write(buf, 0, r);
                    }
                }
                catch (IOException ioe)
                {
                    System.err.println(ioe.getMessage());
                }

                try
                {
                    targetSocket.close();
                    targetInputStream.close();
                    clientOutputStream.close();
                }
                catch (IOException ioe)
                {
                    System.err.println("ioexception thread server client");
                }
            });

            TCPRelay.relayThread(() -> {
                byte[] buf = null;
                int r;

                try
                {
                    buf = new byte[clientSocket.getReceiveBufferSize()];
                }
                catch (SocketException se)
                {
                    System.err.println(se.getMessage());
                }

                do
                {
                    try
                    {
                        r = clientInputStream.read(buf);
                        if (r > 0)
                            targetOutputStream.write(buf, 0, r);
                    }
                    catch (IOException ioe)
                    {
                        System.err.println(ioe.getMessage());
                        r = -1;
                    }
                }
                while (r > 0);

                try
                {
                    clientSocket.close();
                    clientInputStream.close();
                    targetOutputStream.close();
                }
                catch (IOException ioe)
                {
                    System.err.println("ioexception thread server client");
                }
            });

        }
    }

    /**
     * Hauptprogramm. Als Kommandozeilenargumente sollen die Portnummer für die
     * Annahme von Verbindungen sowie Adresse und Port für die Weiterleitung
     * angegeben werden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Öffnen des
     * 
     *             {@link ServerSocket}s zur Annahme von Verbindungen zu einem
     * 
     *             Ein-/Ausgabefehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final int inPort = Integer.parseInt(args[0]);
        final InetAddress tHostAddress = InetAddress.getByName(args[1]);
        final int tPort = Integer.parseInt(args[2]);
        try (final ServerSocket sSock = new ServerSocket(inPort))
        {
            final TCPRelay relay = new TCPRelay(sSock, new InetSocketAddress(tHostAddress, tPort));
            relay.startRelaying();
        }
    }
}
