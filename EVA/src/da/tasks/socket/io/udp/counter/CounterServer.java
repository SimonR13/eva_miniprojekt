package da.tasks.socket.io.udp.counter;

import java.io.IOException;
import java.net.DatagramSocket;

public class CounterServer
{

    private static int counter;

    public static void runServer(final DatagramSocket dgSocket) throws NullPointerException
    {
        ModUdpSocket udpSocket = null;
        String request;
        // von aussen eingegebene ressourcen nicht schliessen!
        try
        {
            udpSocket = new ModUdpSocket(dgSocket);
            while (true)
            {
                try
                {
                    // auf Anfrage warten, in request-String ablegen ...
                    request = udpSocket.receive();
                    // ... verarbeiten und dem client als antwort schicken.
                    udpSocket.reply(processRequestAndReturnAnswer(request));

                }
                catch (IOException ioexp)
                {
                    System.err.println("udpSocket.recieve() threw IOException! Continue.");
                    continue;
                }
            }
        }
        catch (NullPointerException npe)
        {
            System.err.println("Nullpointer");
        }
        finally
        {
            if (udpSocket != null)
            {
                udpSocket = null;
            }
        }
    }

    public static void main(final String[] args) throws IOException
    {
        final int desiredServerPort = Integer.parseInt(args[0]);
        try (final DatagramSocket sock = new DatagramSocket(desiredServerPort))
        {
            CounterServer.runServer(sock);
        }
    }

    private static String processRequestAndReturnAnswer(final String request)
    {
        if (request.equals("increment"))
        {
            CounterServer.counter++;
        }
        else if (request.equals("decrement"))
        {
            CounterServer.counter--;
        }
        else if (request.equals("reset"))
        {
            CounterServer.counter = 0;
        }
        else if (request.startsWith("set"))
        {
            int setCounter = 0;
            try
            {
                String tmp = request.substring(4);
                setCounter = Integer.parseInt(tmp);
            }
            catch (NumberFormatException nfe)
            {
                System.err.println("Falsches 'set X'-Format!");
                return String.valueOf(CounterServer.counter);
            }
            CounterServer.counter = setCounter;
        }

        return String.valueOf(CounterServer.counter);
    }

}
