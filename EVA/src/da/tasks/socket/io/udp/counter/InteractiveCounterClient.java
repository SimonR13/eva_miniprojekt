package da.tasks.socket.io.udp.counter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import da.lecture.socket.io.udp.UDPSocket;

public class InteractiveCounterClient
{

    private static final int TIMEOUT = 2000;

    private final static BufferedReader SYSIN = new BufferedReader(new InputStreamReader(System.in));

    private static InetAddress ipAddressServer;

    private static int portServer;

    private static UDPSocket udpSocket;

    public static void main(String[] args)
    {

        if (args.length > 1)
        {
            System.out.println("Verbindungsdetails wurden als Parameter uebergeben.");
            try
            {
                ipAddressServer = InetAddress.getByName(args[0]);
                portServer = Integer.parseInt(args[1]);
            }
            catch (Exception exp)
            {
                System.err.println("Fehler beim Einlesen der Verbindungsparameter! Weiter mit interaktiver eingaben.");
                args = null;
            }
        }
        if (args == null || args.length < 1)
        {
            try
            {
                // Serveradresse einlesen und in InetAddress umwandeln ...
                System.out.print("Serveradresse [localhost]:");
                String tmp = SYSIN.readLine();
                ipAddressServer = InetAddress.getByName(tmp.equals("") ? "localhost" : tmp);
                // ... Serverport einlesen und parsen ...
                System.out.print("Serverport [1234]:");
                tmp = SYSIN.readLine();
                portServer = tmp.equals("") ? 1234 : Integer.parseInt(tmp);
            }
            catch (Exception ex)
            {
                System.err.println("Fehler bei eingabe!");
            }
            System.out.println("Verbindung mit Server wird aufgebaut...");
        }
        if (ipAddressServer != null && portServer > 0)
        {
            if (InteractiveCounterClient.connectToServer())
            {
                System.out.println("Verbindung erfolgreich hergestellt! Bitte geben Sie den ersten Befehl ein:");

                boolean notQuit = true, waitForResponse = false;

                while (notQuit)
                {
                    try
                    {
                        String input = SYSIN.readLine();
                        String action[] = input.split(" ", 2);
                        switch (action[0])
                        {
                            case "enter":
                                System.out.println("Send enterPoll " + action[1]);
                                udpSocket.send("enterPoll " + action[1], ipAddressServer, portServer);
                                waitForResponse = true;
                                break;
                            case "inc":
                                System.out.println("Send increment ...");
                                udpSocket.send("incrementVotes " + action[1], ipAddressServer, portServer);
                                waitForResponse = true;
                                break;
                            case "dec":
                                System.out.println("Send decrement ...");
                                udpSocket.send("decrement", ipAddressServer, portServer);
                                waitForResponse = true;
                                break;
                            case "set":
                                System.out.println("Send set " + action[1] + "...");
                                udpSocket.send("set " + action[1], ipAddressServer, portServer);
                                waitForResponse = true;
                                break;
                            case "reset":
                                System.out.println("Send reset ...");
                                udpSocket.send("reset", ipAddressServer, portServer);
                                waitForResponse = true;
                                break;
                            case "help":
                                System.out.println("Moegliche Befehle sind:");
                                System.out.println("increment, decrement, set [n], help, quit");
                                break;
                            case "quit":
                                System.out.println("Verbindung wird geschlossen ...");
                                udpSocket.close();
                                System.out.println("... und Programm wird beendet!");
                                notQuit = false;
                                break;
                            default:
                                System.out.println("Konnte keine Befehl erkennen. 'help' fuer Liste mit Befehlen.");
                        }
                        if (waitForResponse)
                        {
                            System.out.println("... wait for response ...");
                            String response = udpSocket.receive();
                            System.out.println("... response: " + response);
                            waitForResponse = false;
                        }

                    }
                    catch (SocketTimeoutException ste)
                    {
                        System.err.println("Anwort vom Server dauerte zu Lange. Nachrichtenverlust! Erneut versuchen...");
                    }
                    catch (IOException ioe)
                    {
                        System.err.println("Befehl konnte nicht gesendet werden! Bitte erneut versuchen...");
                    }
                    catch (Exception ex)
                    {
                        System.err.println("Fehler beim Einlesen. Nochmal versuchen...");
                        continue;
                    }
                }

            }
            else
            {
                System.err.println("Fehler beim Aufbau der Verbindung! Bitte neu starten.");
            }
        }

    }

    private static boolean connectToServer()
    {
        if (ipAddressServer != null && portServer > 0)
        {
            if (udpSocket == null)
            {
                try
                {
                    udpSocket = new UDPSocket();
                    udpSocket.setTimeout(TIMEOUT);
                }
                catch (SocketException se)
                {
                    System.err.println("SocketException beim oeffnen des Sockets!");
                    return false;
                }
                return true;
            }
        }
        return false;
    }

}
