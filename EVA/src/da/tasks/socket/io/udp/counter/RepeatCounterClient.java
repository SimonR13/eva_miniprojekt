package da.tasks.socket.io.udp.counter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import da.lecture.socket.io.udp.UDPSocket;

public class RepeatCounterClient
{

    private final static int TIMEOUT = 1000;

    private final static int MAX_REPEAT = 2;

    private static InetAddress ipAddressServer;

    private static int portServer;

    private static int incCount;

    public static void main(String[] args) throws IOException
    {

        try (final UDPSocket udpSocket = new UDPSocket())
        {
            try
            {
                ipAddressServer = InetAddress.getByName(args[0]);
                portServer = Integer.parseInt(args[1]);
                incCount = Integer.parseInt(args[2]);
            }
            catch (Exception exp)
            {
                System.err.println("Fehler beim uebergeben der Parameter! Exit. \n" + exp.getMessage());
            }

            udpSocket.setTimeout(RepeatCounterClient.TIMEOUT);

            String action = "reset";
            for (int i = 0; i <= incCount; i++)
            {
                if (i > 0)
                {
                    action = "increment";
                }
                for (int j = (RepeatCounterClient.MAX_REPEAT); j >= 0; --j)
                {
                    // increment befehl senden...
                    udpSocket.send(action, ipAddressServer, portServer);
                    try
                    {
                        // ... auf antwort des servers warten
                        System.out.println("Antwort: " + udpSocket.receive());
                        break;
                    }
                    catch (final SocketTimeoutException ste)
                    {
                        System.out.println("ACHTUNG: Vermutlich Nachrichtenverlust aufgetreten! " + "Versuche uebrig: " + j);
                        if (j == 0)
                        {
                            throw new TimeoutException("Zu viele Timeouts!");
                        }
                        continue;
                    }
                }
            }
        }
        catch (TimeoutException te)
        {
            System.out.println();
            System.err.println(te.getMessage());
        }

    }

}
