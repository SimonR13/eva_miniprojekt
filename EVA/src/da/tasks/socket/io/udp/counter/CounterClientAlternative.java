package da.tasks.socket.io.udp.counter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

import da.lecture.socket.io.udp.UDPSocket;

public class CounterClientAlternative
{

    private static final int TIMEOUT = 1000;

    private static InetAddress ipAddressServer;

    private static int portServer;

    private static int incCount;

    public static void main(String[] args) throws IOException
    {
        try (final UDPSocket udpSocket = new UDPSocket())
        {
            udpSocket.setTimeout(CounterClientAlternative.TIMEOUT);

            try
            {
                ipAddressServer = InetAddress.getByName(args[0]);
                portServer = Integer.parseInt(args[1]);
                incCount = Integer.parseInt(args[2]);
            }
            catch (Exception exp)
            {
                System.err.println("Fehler beim uebergeben der Parameter! Exit. \n" + exp.getMessage());
            }

            String action = "reset";

            for (int i = 0; i <= incCount; i++)
            {
                if (i > 0)
                {
                    action = "increment";
                }
                // increment befehl senden...
                udpSocket.send(action, ipAddressServer, portServer);
                // ... auf antwort des servers warten
                System.out.println("Antwort: " + udpSocket.receive());
            }
        }
        catch (final SocketTimeoutException ste)
        {
            System.out.println("ACHTUNG: Vermutlich Nachrichtenverlust aufgetreten!");
        }
    }

}
