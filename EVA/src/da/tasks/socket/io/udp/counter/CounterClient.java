package da.tasks.socket.io.udp.counter;

import java.net.InetAddress;
import java.net.SocketTimeoutException;

import da.lecture.socket.io.udp.UDPSocket;

public class CounterClient
{
    /**
     * Anzahl an Millisekunden, die auf eine Antwort vom {@link CounterServer}
     * gewartet werden soll.
     */
    private static final int TIMEOUT = 2000;

    private static InetAddress ipAddressServer;

    private static int portServer;

    private static int incCount;

    public static void main(String[] args) throws Exception
    {
        try
        {
            ipAddressServer = InetAddress.getByName(args[0]);
            portServer = Integer.parseInt(args[1]);
            incCount = Integer.parseInt(args[2]);
        }
        catch (Exception exp)
        {
            System.err.println("Fehler beim uebergeben der Parameter! Exit. \n" + exp.getMessage());
        }
        
        try (final UDPSocket udpSocket = new UDPSocket())
        {
            udpSocket.setTimeout(TIMEOUT);

            udpSocket.send("reset", ipAddressServer, portServer);

            for (int i = 0; i < incCount; i++)
            {
                udpSocket.send("increment", ipAddressServer, portServer);
            }

            for (int i = 0; i < (incCount + 1); i++)
            {
                System.out.println("Antwort: " + udpSocket.receive());
            }
        }
        catch (final SocketTimeoutException ste)
        {
            System.out.println("ACHTUNG: Vermutlich Nachrichtenverlust aufgetreten!");
        }

    }

}
