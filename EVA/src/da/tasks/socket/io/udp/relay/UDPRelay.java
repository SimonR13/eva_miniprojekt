package da.tasks.socket.io.udp.relay;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import da.lecture.socket.io.udp.UDPSocket;
import da.tasks.socket.io.udp.counter.ModUdpSocket;

/** UDP-Relay zur bedingten Weiterleitung von UPD-Datagrammen. */
public class UDPRelay
{
    /**
     * Methode zum Starten des UDP-Relays.
     * 
     * @param inSocket
     *            {@link DatagramSocket}, über das Datagramme zum Weiterleiten
     *            empfangen werden sollen.
     * 
     * @param addressToRelayTo
     *            Adresse des Rechners, an den die über inSocket empfagenen
     *            Datagramme weitergeleitet werden sollen.
     * 
     * @param portToRelayTo
     *            Portnummer, an die die über inSocket empfagenen Datagramme
     *            weitergeleitet werden sollen.
     * 
     * @param controlReader
     *            {@link Reader} von dem gelesen werden soll, ob ein über
     *            inSocket empfangenes Datagramm weitergeleitet oder verworfen
     *            werden soll.
     * 
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der UDP-Kommunikation zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     * 
     */
    public static void startUDPRelay(final DatagramSocket inSocket, final InetAddress addressToRelayTo, final int portToRelayTo, final Reader controlReader) throws IOException
    {
        try (ModUdpSocket udpSocket = new ModUdpSocket(inSocket))
        {
            final BufferedReader bufReader = new BufferedReader(controlReader);
            while (true)
            {
                boolean messageDismissed = false;

                // auf eingehende nachricht warten
                String incommingMessage = udpSocket.receive();
                // sender der nachricht ermitteln
                String replayClient[] = udpSocket.getSenderOfLastReceivedString().split(":", 2);
                int replayClientPort = Integer.parseInt(replayClient[1]);

                // replayClientPort != portToRelayTo => msg von client
                if (replayClientPort != portToRelayTo)
                {
                    String server2Client = "";
                    System.out.println("Message from Client to Server: " + incommingMessage);
                    System.out.print("Forward ([j]/n): ");

                    int forward = bufReader.readLine().charAt(0);
                    switch (forward)
                    {
                        case 84:
                            // handled by next case
                        case 110:
                            System.out.println("Not Forwarding Message.");
                            messageDismissed = true;
                            break;
                        case 80:
                            // handled by default
                        case 106:
                            // handled by default
                        default:
                            System.out.println("Forward Datagram...");
                            server2Client = UDPRelay.forwardDatagram(incommingMessage, addressToRelayTo, portToRelayTo, bufReader);
                    }

                    if (messageDismissed || server2Client == null)
                    {
                        continue;
                    }

                    udpSocket.reply(server2Client);
                    System.out.println("Message sent.");
                }
            }
        }
        catch (SocketException sockExp)
        {
            System.err.println("Fehler beim Erstellen des Sockets!" + sockExp.getMessage());
        }
    }

    private static String forwardDatagram(String incommingMessage, InetAddress addressToRelayTo, int portToRelayTo, BufferedReader controlReader)
    {
        try (UDPSocket relayServerSocket = new UDPSocket())
        {
            relayServerSocket.send(incommingMessage, addressToRelayTo, portToRelayTo);
            System.out.println("... waiting for Message from Server to Client...");
            String server2Client = relayServerSocket.receive();

            System.out.println("Message from Server to Client: " + server2Client);
            System.out.print("Forward ([j]/n): ");

            int forward = controlReader.readLine().charAt(0);
            switch (forward)
            {
                case 84:
                    // handled by next case
                case 110:
                    System.out.println("Not Forwarding Message.");
                    return null;
                case 80:
                    // handled by default
                case 106:
                    // handled by default
                default:
                    System.out.println("Forward Datagram...");
                    return server2Client;
            }

        }
        catch (SocketException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Hauptprogramm. Als Kommandozeilenargumente sollen die Portnummer für den
     * Empfang von Datagrammen sowie Adresse und Port für die Weiterleitung der
     * empfangenen Datagramme angegeben werden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der UDP-Kommunikation zu einem
     *             Ein-/Ausgabefehler gekommen ist.
     * 
     */
    public static void main(final String[] args) throws IOException
    {
        final int inPort = Integer.parseInt(args[0]);
        final InetAddress addressToRelayTo = InetAddress.getByName(args[1]);
        final int portToRelayTo = Integer.parseInt(args[2]);
        final Reader controlReader = new InputStreamReader(System.in);
        try (final DatagramSocket dgSocket = new DatagramSocket(inPort))
        {
            UDPRelay.startUDPRelay(dgSocket, addressToRelayTo, portToRelayTo, controlReader);
        }
    }
}
