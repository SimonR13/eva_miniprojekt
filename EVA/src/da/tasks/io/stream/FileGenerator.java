package da.tasks.io.stream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/*Entwickeln Sie ein Programm, mit dem Sie eine Datei einer beliebigen Größe erzeugen.
 Geben Sie den Dateinamen der Datei sowie die Anzahl der Bytes als Kommandozeilenar-
 gumente an. Ihr Programm soll dann diese Datei erzeugen und entsprechend viele Bytes
 in die Datei schreiben. Der Inhalt der Datei ist beliebig. Prüfen Sie nach, ob Ihre Datei
 am Ende genau so groß ist, wie sie sein soll.
 */

public class FileGenerator
{

    public static void main(String[] args)
    {
        String file = "";
        int byteCount = 0;
        // parameter einlesen
        if (args.length >= 2)
        {
            file = args[0];
            byteCount = Integer.parseInt(args[1]);
        }
        else
        {
            System.err.println("Fehlende Kommandozeilenparameter!");

        }

        try (FileOutputStream os = new FileOutputStream(file))
        {
            // Random-Objekt initialisieren
            Random randm = new Random();
            // Byte-Array erstellen
            byte[] bytes = new byte[byteCount];

            // array mit willkuerlichen daten fuellen
            randm.nextBytes(bytes);

            // schreiben der byte in datei
            os.write(bytes);
            // durchspuelen
            os.flush();
        }
        catch (FileNotFoundException e)
        {
            System.err.println("Datei nicht gefunden...");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            System.err.println("IN/OUT Fehler...");
            e.printStackTrace();
        }

        System.out.println("Datei :" + file + " mit " + byteCount + " Bytes wurde erstellt!");

    }

}
