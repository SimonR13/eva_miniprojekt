package da.tasks.io.stream.serialization.xstream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.thoughtworks.xstream.XStream;

import da.tasks.io.stream.serialization.linkedlist.LinkedList;
import da.tasks.io.stream.serialization.linkedlist.ListEntry;

public class XStreamSerialization
{
    /*  
     * ListEntry.next transient entfernen!
     */

    private final static int LIST_LENGTH = 10;

    private final static LinkedList LINKED_LIST = new LinkedList();

    private final static String FILE = "xstream.out";

    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        // initialisieren des XStream
        XStream xstr = new XStream();
        xstr.alias("liste", LinkedList.class);
        xstr.alias("entry", ListEntry.class);

        // Erstellen eine kurzen einfach-verketteten List
        // gleichzeitiges serialisieren
        for (int i = 0; i < LIST_LENGTH; i++)
        {
            LINKED_LIST.add(i + "");
        }

        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(FILE)))
        {
            // ausgabe der LINKED_LIST in die datei FILE
            xstr.toXML(LINKED_LIST, bos);
        }

        LinkedList readList = null;
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(FILE)))
        {
            readList = (LinkedList) xstr.fromXML(bis);
        }

        System.out.println("Original LINKED_LIST: ");
        System.out.println(LINKED_LIST);
        System.out.println("Deserialized LINKED_LIST:");
        System.out.println(readList);

    }

}
