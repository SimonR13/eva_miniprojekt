package da.tasks.io.stream.serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import da.tasks.io.stream.serialization.linkedlist.LinkedList;

public class ObjectCopy
{
    /*  
     * ListEntry.next transient entfernen!
     */

    private static LinkedList origListe = new LinkedList();

    private static LinkedList copyListe = null;

    private static byte[] retVal = null;

    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        for (int i = 0; i < 200; i++)
        {
            origListe.add(i + "");
        }

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos))
        {
            oos.writeObject(origListe);
            retVal = baos.toByteArray();
        }

        try (ByteArrayInputStream bais = new ByteArrayInputStream(retVal);
        ObjectInputStream ois = new ObjectInputStream(bais))
        {
            copyListe = (LinkedList) ois.readObject();
        }

        System.out.println("Original List: ");
        System.out.println(origListe.toString());
        System.out.println("Copied List: ");
        System.out.println(copyListe.toString());
    }
}
