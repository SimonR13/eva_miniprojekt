package da.tasks.io.stream.serialization.linkedlist;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * Beispielhafte Klasse zum Serialisieren und Deserialisieren einer
 * einfachverketteten Liste vom Typ {@link LinkedList}.
 */
public class LinkedListSerialization
{
    /** Name der temporären Datei, die als Zwischenspeicher dienen soll. */
    public static final File TEMPFILE = new File("oOut.tmp");

    /**
     * Anzahl an Einträgen, die der einfachverketteten Liste vor der
     * Serialisierung hinzugefügt werden sollen.
     */
    public static final int NUMBER_OF_ELEMENTS_TO_ADD = 10000;

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es während des Serialisierungs-
     * 
     *             prozesses zu einem Ein-/Ausgabefehler gekommen ist.
     * @throws ClassNotFoundException
     *             Wird ausgelöst, wenn bei der Deseriali-
     * 
     *             sierung eine Klasse benötigt wird, die nicht gefunden werden
     *             konnte.
     */
    public static void main(final String[] args) throws IOException, ClassNotFoundException
    {
        final LinkedList stringList = new LinkedList();
        for (int i = 0; i < NUMBER_OF_ELEMENTS_TO_ADD; i++)
        {
            stringList.add(String.valueOf(i));
        }
        System.out.println("Vor der Serialisierung:");
        System.out.println(stringList.toString());
        try (final LinkedListObjectOutputStream oOut = new LinkedListObjectOutputStream(new BufferedOutputStream(new FileOutputStream(TEMPFILE))))
        {
            oOut.writeLinkedList(stringList);
        }
        try (final LinkedListObjectInputStream oIn = new LinkedListObjectInputStream(new BufferedInputStream(new FileInputStream(TEMPFILE))))
        {
            final LinkedList copyList = oIn.readLinkedList();
            System.out.println("Nach der Deserialisierung:");
            System.out.println(copyList.toString());

        }
    }

    private static class LinkedListObjectOutputStream extends ObjectOutputStream
    {

        public LinkedListObjectOutputStream(OutputStream out) throws IOException
        {
            super(out);
            // nothing todo
        }

        public void writeLinkedList(LinkedList ll) throws IOException
        {
            // Tail als Referenz fuer listen-ende setzen
            ListEntry tmp = ll.getTail();
            super.writeObject(tmp);

            // Head als tmp-Entry setzen
            tmp = ll.getHead();
            // solange ein Entry vorhanden ist...
            while (tmp != null)
            {
                // ... ListEntry schreiben ...
                super.writeObject(tmp);
                // ... folgendes Entry als tmp setzen
                tmp = tmp.getNext();
            }
        }

    }

    private static class LinkedListObjectInputStream extends ObjectInputStream
    {

        public LinkedListObjectInputStream(InputStream in) throws IOException
        {
            super(in);
            // nothing todo
        }

        public LinkedList readLinkedList() throws ClassNotFoundException, IOException
        {
            ListEntry tmp = null;
            LinkedList retVal = new LinkedList();
            // Referenz fuer listen-ende
            ListEntry tail = (ListEntry) super.readObject();

            // Head-Object lesen
            tmp = (ListEntry) super.readObject();

            // head in LinkedList setzen und alle weiteren werte anfuegen
            do
            {
                retVal.add(tmp.getValue());
                tmp = (ListEntry) super.readObject();
                if (tail == tmp || tail.equals(tmp) || tail.getValue().equals(tmp.getValue()))
                {
                    tmp = null;
                }
            }
            while (tmp != null);

            return retVal;
        }

    }
}
