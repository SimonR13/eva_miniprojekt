package da.tasks.io.stream.serialization.linkedlist;

import java.io.Serializable;

/** Einfachverkettete Liste bestehend aus {@link ListEntry}-Objekten. */
public class LinkedList implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -6335920504053320340L;

    /** Erstes Listenelement. */
    private ListEntry head;

    public ListEntry getHead()
    {
        return head;
    }

    /** Letztes Listenelement. */
    private ListEntry tail;

    public ListEntry getTail()
    {
        return tail;
    }

    /**
     * Fügt den übergebenen Wert an das Ende der einfachverketten Liste an.
     * 
     * @param value
     *            Anzufügender Wert.
     */
    public void add(final String value)
    {
        final ListEntry newEntry = new ListEntry(value);
        if ((this.head == null) && (this.tail == null))
        {
            this.head = newEntry;
        }
        else
        {
            this.tail.setNext(newEntry);
        }
        this.tail = newEntry;
    }
    
    @Override
    public String toString() 
    {
        final StringBuilder builder = new StringBuilder("Listeninhalt: \n");
        ListEntry currentEntry = this.head;
        while (currentEntry != null) 
        {
            builder.append(currentEntry.getValue()).append("\n");
            currentEntry = currentEntry.getNext();
        }
        return builder.toString();
    }
}
