package da.tasks.io.stream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.Path;

public class FileCopyMeasuring
{
    // erwartet als Parameter [fileIn] [fileOut] [int Buffer]
    public static void main(String[] args) throws IOException
    {
        if (args.length < 2)
        {
            System.err.println("Kein Argument fuer Datei uebergeben...");
            System.out.println("Starte neu mit Standardwerten");
            String[] newArgs = new String[]
            { "bytes.txt", "bytesOut.txt", "4" };
            FileCopyMeasuring.main(newArgs);

        }
        else
        {
            final String fileIn = args[0];
            final String fileOut = args[1];
            final int buffer = Integer.parseInt(args[2]);

            Long timeFileStream = -1L, timeBufferedStream = -1L, timeByteFileStream = -1L, timeFileChannel = -1L;
            try
            {
                timeFileStream = measureFileInputStream(fileIn, fileOut);
            }
            catch (Exception e)
            {
                System.err.println("Fehler beim Messen der FileInputStream-Zeit!" + e);
            }

            try
            {
                timeBufferedStream = measureBufferedInputStream(fileIn, fileOut);
            }
            catch (Exception e)
            {
                System.err.println("Fehler beim Messen der BufferedInputStream-Zeit!" + e);
            }

            try
            {
                timeByteFileStream = measureFileInputStreamBytes(fileIn, fileOut, buffer);
            }
            catch (Exception e)
            {
                System.err.println("Fehler beim Messen der FileInputStreamBytes-Zeit!" + e);
            }

            try
            {
                timeFileChannel = measureFileChannel(fileIn, fileOut);
            }
            catch (Exception e)
            {
                System.err.println("Fehler beim Messen der FileInputStreamBytes-Zeit!" + e);
            }

            System.out.println("FileStream: " + timeFileStream + " Millisekunden");
            System.out.println("BufferedStream: " + timeBufferedStream + " Millisekunden");
            System.out.println("ByteFileStream: " + timeByteFileStream + " Millisekunden");
            System.out.println("FileChannel: " + timeFileChannel + " Millisekunden");
        }

    }

    public static Long measureFileInputStream(final String fileIn, final String fileOut) throws FileNotFoundException, IOException
    {
        Long start = -1L, end = -1L;
        try (FileInputStream is = new FileInputStream(fileIn);
        FileOutputStream os = new FileOutputStream(fileOut))
        {
            start = System.currentTimeMillis();
            int data = 0;
            while ((data = is.read()) != -1)
            {
                os.write(data);
            }
            end = System.currentTimeMillis();
        }

        return (end - start);
    }

    public static Long measureBufferedInputStream(final String fileIn, final String fileOut) throws FileNotFoundException, IOException
    {
        Long start = -1L, end = -1L;
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(fileIn));
        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(fileOut)))
        {
            start = System.currentTimeMillis();
            int data = 0;
            while ((data = is.read()) != -1)
            {
                os.write(data);
            }
            end = System.currentTimeMillis();
        }

        return (end - start);
    }

    public static Long measureFileInputStreamBytes(final String fileIn, final String fileOut, final int buffer) throws FileNotFoundException, IOException
    {
        Long start = -1L, end = -1L;
        try (FileInputStream is = new FileInputStream(fileIn);
        FileOutputStream os = new FileOutputStream(fileOut))
        {
            start = System.currentTimeMillis();
            byte[] dataBuff = new byte[buffer];
            while ((is.read(dataBuff)) != -1)
            {
                for (int i = 0; i < dataBuff.length - 1; i++)
                {
                    os.write(dataBuff, i, 1);
                }
            }
            end = System.currentTimeMillis();
        }

        return (end - start);
    }

    public static Long measureFileChannel(final String fileIn, final String fileOut)
    {
        Long start = -1L;

        final Path pathIn = FileSystems.getDefault().getPath("", fileIn);
        final Path pathOut = FileSystems.getDefault().getPath("", fileOut);

        start = System.currentTimeMillis();
        try (final FileChannel fcIn = FileChannel.open(pathIn, new OpenOption[]
        { StandardOpenOption.READ });
        final FileChannel fcOut = FileChannel.open(pathOut, new OpenOption[]
        { StandardOpenOption.CREATE, StandardOpenOption.WRITE }))
        {
            fcIn.transferTo(0, fcIn.size(), fcOut);
        }
        catch (Exception exp)
        {
            System.err.println("Exception LOL :P");
        }
        
        return (System.currentTimeMillis() - start);
    }

}
