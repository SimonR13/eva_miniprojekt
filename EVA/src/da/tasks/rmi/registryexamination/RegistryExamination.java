package da.tasks.rmi.registryexamination;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import da.tasks.rmi.list.reference.List;

public class RegistryExamination 
{

    public static void main(String[] args) throws RemoteException, AlreadyBoundException, NotBoundException
    {
        final Registry localReg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        final List obj = new List(23, 42, 5);
        localReg.bind("TestObjekt", obj);
        
        System.out.println("lookup classname: " + localReg.lookup("TestObjekt").getClass().getName());
        
    }

}
