package da.tasks.rmi.restrictstub.socket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketImpl;

public class RestrictionServerSocket extends ServerSocket
{

    public RestrictionServerSocket() throws IOException
    {
        // TODO Auto-generated constructor stub
    }

    public RestrictionServerSocket(int port) throws IOException
    {
        super(port);
        // TODO Auto-generated constructor stub
    }

    public RestrictionServerSocket(int port, int backlog) throws IOException
    {
        super(port, backlog);
        // TODO Auto-generated constructor stub
    }

    public RestrictionServerSocket(int port, int backlog, InetAddress bindAddr) throws IOException
    {
        super(port, backlog, bindAddr);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public Socket accept() throws IOException {
        if (isClosed())
            throw new SocketException("Socket is closed");
        if (!isBound())
            throw new SocketException("Socket is not bound yet");
        final Socket s = new RestrictionSocket((SocketImpl) null);
        this.implAccept(s);
        InetAddress localhost = InetAddress.getByName("localhost");
        System.out.println("Socket.Hostname: " + s.getInetAddress().getHostAddress());
        if(!s.getInetAddress().getHostAddress().equals(localhost.getHostAddress())) {
            System.err.println("Unerlaubter Zugriff!");
            throw new SocketException("Unerlaubter Zugriff!");
        }
        return s;
    }
}
