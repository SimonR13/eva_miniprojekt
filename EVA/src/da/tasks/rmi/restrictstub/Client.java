package da.tasks.rmi.restrictstub;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import da.lecture.rmi.exportexamination.SomeRMIInterface;
import da.lecture.rmi.exportexamination.SomeRMIInterfaceImpl;

/**
 * Client-Klasse zur Veranschaulichung der Wirkung von {@link UnicastRemoteObject#exportObject(java.rmi.Remote, int)}.
 */
public class Client {
    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws RemoteException Wird ausgelöst, wenn es zu einem RMI spezifischen Fehler gekommen ist.
     * @throws NotBoundException Wird ausgelöst, wenn in einer Registry kein {@link SomeRMIInterface} an den Namen
     *             {@link SomeRMIInterface#DEFAULT_RMI_OBJECT_NAME} gebunden ist.
     */
    public static void main(final String[] args) throws RemoteException, NotBoundException {
        final Registry localReg = LocateRegistry.getRegistry("Wheezy-ThinkPad");
        final SomeRMIInterface someObj = (SomeRMIInterface) localReg.lookup(SomeRMIInterface.DEFAULT_RMI_OBJECT_NAME);

        final SomeRMIInterfaceImpl someOtherObj = new SomeRMIInterfaceImpl();
//        someObj.someMethod(someOtherObj); // someOtherObj noch nicht exportiert, daher hier Wertübergabe
//        UnicastRemoteObject.exportObject(someOtherObj, 0);

        someObj.someMethod(someOtherObj); // someOtherObj jetzt exportiert, daher Referenzübergabe
    }
}
