package da.tasks.rmi.restrictstub.factory;

import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;

import da.tasks.rmi.restrictstub.socket.RestrictionServerSocket;
import da.tasks.rmi.restrictstub.socket.RestrictionSocket;

@SuppressWarnings("serial")
public class RestrictionSocketFactory implements RMIClientSocketFactory, RMIServerSocketFactory, Serializable
{

    @Override
    public ServerSocket createServerSocket(final int port) throws IOException
    {
        return new RestrictionServerSocket(port);
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException
    {
        return new RestrictionSocket(host, port);
    }
    
}
