package da.tasks.rmi.compressexamination.streams;

import java.io.IOException;
import java.io.OutputStream;

public class FilterOutputStream extends OutputStream
{
    
    private OutputStream _os;
    private int _byteCounter = 0;
    
    public FilterOutputStream(OutputStream os)
    {
        _os = os;
    }

    @Override
    public void write(int b) throws IOException
    {
        _os.write(b);
        this.incCounter();

    }
    
    private void incCounter() {
        _byteCounter++;
        System.out.println(this.getClass().getName() + " bytes counted: " + _byteCounter);
    }

}
