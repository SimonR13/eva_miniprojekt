package da.tasks.rmi.compressexamination.streams;

import java.io.IOException;
import java.io.InputStream;

public class FilterInputStream extends InputStream
{

    private InputStream _is;
    private int _byteCounter = 0;
    
    public FilterInputStream(InputStream is)
    {
        _is = is;
    }
    
    @Override
    public int read() throws IOException
    {
        int data;
        while ((data = _is.read()) != -1) {
            this.incCounter();
            return data;
        }
        return -1;
    }
    
    private void incCounter(){
        _byteCounter++;
        System.out.println(this.getClass().getName() + " bytes counted: " + _byteCounter);
    }

}
