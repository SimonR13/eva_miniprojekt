package da.tasks.rmi.list.reference;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * RMI-Schnittstelle für ein RMI-Objekt, welches Werte an eine Liste anhängt.
 */
public interface RemoteAppender extends Remote {
    /** Konstante für den Standardnamen eines RemoteAppender-Objektes in einer RMI-Registry. */
    String DEFAULT_RMI_OBJECT_NAME = "RemoteAppender";

    /**
     * Hängt den übergebenen Wert an das Ende der übergebenen Liste an.
     * @param listToAppendValueTo Liste, an die angehängt werden soll.
     * @param valueToAppend Wert, der an die Liste angehängt werden soll.
     * @return 
     * @throws RemoteException Wird ausgelöst, wenn es bei der RMI-Kommunikation zu einem Fehler gekommen ist.
     */
    void appendValue(RMIList listToAppendValueTo, int valueToAppend) throws RemoteException;
}
