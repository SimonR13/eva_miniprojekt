package da.tasks.rmi.list.reference;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Client-Klasse, die ein {@link RemoteAppender}-Objekt von einer über {@link LocateRegistry#getRegistry()} angesprochenen
 * RMI-Registry bezieht und darauf verschiedene Methodenaufrufe ausführt.
 */
public class Client {
    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Adresse des Rechners angegeben werden, auf dem die RMI-Registry
     * läuft.
     * @param args Kommandozeilenargumente.
     * @throws RemoteException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     * @throws NotBoundException Wird ausgelöst, wenn bei der RMI-Registry auf dem Rechner mit der übergebenen Adresse kein Objekt
     *             mit dem Namen {@link RemoteAppender#DEFAULT_RMI_OBJECT_NAME} registriert ist.
     */
    public static void main(final String[] args) throws RemoteException, NotBoundException {
        final List list = new List(0, 8, 15);
        final Registry localReg = LocateRegistry.getRegistry("localhost");
        final RemoteAppender appender = (RemoteAppender) localReg.lookup(RemoteAppender.DEFAULT_RMI_OBJECT_NAME);

        System.out.println("before: " + list.toString()); // Ausgabe [0 8 15]
        appender.appendValue(list, 4711);
        System.out.println("after: " + list.toString()); // Ausgabe ?
    }
}
