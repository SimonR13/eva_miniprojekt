package da.tasks.rmi.list.reference;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIList extends Remote
{
    String DEFAULT_RMI_OBJECT_NAME = "RMIList";
    
    void appendValue(int valueToAppend) throws RemoteException;
    
}
