package da.tasks.rmi.list.valueresult;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Implementierung von {@link RemoteAppender} zu Manipulieren eines {@link List}-Objektes.
 */
@SuppressWarnings("serial")
public class RemoteAppenderImpl extends UnicastRemoteObject implements RemoteAppender {

    /**
     * Initialisiert eine neue RemoteAppenderImpl-Instanz.
     * @throws RemoteException Wird ausgelöst, wenn es beim Exportieren des RMI-Objektes, also beim Erzeugen des Skeletons, zu
     *             einem Fehler gekommen ist.
     */
    public RemoteAppenderImpl() throws RemoteException {
        // Muss wegen RemoteException von super() zur Verfügung gestellt werden.
    }

    @Override public List appendValue(final List listToAppendValueTo, final int valueToAppend) throws RemoteException {
        listToAppendValueTo.append(valueToAppend);
        return listToAppendValueTo;
    }
}
