package da.tasks.rmi.stubexamination;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class StubExamServer
{

    public static void main(String[] args) throws RemoteException, MalformedURLException, AlreadyBoundException
    {
        final StubExamInterface stubExamObjOne = new StubExamClass();
        final StubExamInterface stubExamObjTwo = new StubExamClass();

        Naming.bind("StubExamRMIOne", stubExamObjOne);
        Naming.bind("StubExamRMITwo", stubExamObjTwo);

        System.out.println("StubExamServer is ready!");
    }

}
