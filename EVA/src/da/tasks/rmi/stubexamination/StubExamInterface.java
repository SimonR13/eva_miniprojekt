package da.tasks.rmi.stubexamination;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface StubExamInterface extends Remote
{

    public String getName() throws RemoteException;
    
}
