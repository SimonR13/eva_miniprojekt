package da.tasks.rmi.stubexamination;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class StubExamClass extends UnicastRemoteObject implements StubExamInterface
{
    /**
     * 
     */
    private static final long serialVersionUID = 8836907471413161087L;
    
    protected StubExamClass() throws RemoteException
    {
        // nothing todo
    }

    @Override
    public String getName() throws RemoteException
    {
        return "This is StubExamClass!";
    }

}
