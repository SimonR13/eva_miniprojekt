package da.tasks.rmi.stubexamination;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class StubExamClient
{

    public static void main(String[] args) throws RemoteException, MalformedURLException, NotBoundException
    {
        final String objUrl = String.format("rmi://%s/", "localhost");
        System.out.println("Naming.list fuer: " + objUrl.toString() + "...");
        final String[] objList = Naming.list(objUrl);
        System.out.println("... darin enthaltene Namen: ");
        for (String obj : objList)
        {
            System.out.println(obj.toString());
        }

        System.out.println("2 Stubs vom selben Objekt...");
        Object objOne = Naming.lookup(objList[0]); 
        Object objTwo = Naming.lookup(objList[0]);
        System.out.println("Vergleiche Objekte:");
        System.out.println("objOne == objTwo: " + (objOne == objTwo));
        System.out.println("objOne.equals(objTwo): " + objOne.equals(objTwo));
        
        System.out.println("2 Stubs durch 2 lookups ...");
        objOne = Naming.lookup(objList[0]);
        objTwo = Naming.lookup(objList[1]);
        System.out.println("Vergleiche Objekte:");
        System.out.println("objOne == objTwo: " + (objOne == objTwo));
        System.out.println("objOne.equals(objTwo): " + objOne.equals(objTwo));
        
        System.out.println("DONE");
    }

}
