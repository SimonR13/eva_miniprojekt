package da.tasks.rmi.mult.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;

import da.tasks.rmi.mult.Multiplication;

public class RmiMultiplicationClient
{

    public static void main(String[] args) throws NotBoundException, IOException, ServerNotActiveException
    {
        RemoteServer.setLog(new DataOutputStream(System.out));

        final String objUrl = "rmi://" + (args.length < 1 ? "localhost" : args[0]) + "/" + Multiplication.DEFAULT_RMI_OBJECT_NAME;
        final Multiplication multi = (Multiplication) Naming.lookup(objUrl);

        RmiMultiplicationClient.multiplicationTable(multi);

        System.out.println();
        System.out.println("RMIServer.testClientHost()");
        System.out.println(multi.testClientHost());
        System.out.println("RemoteServer.getClientHost();");
        System.out.println(RemoteServer.getClientHost());
        System.out.println("end");
    }

    private static void multiplicationTable(final Multiplication multi) throws IOException
    {
        System.out.println("Bitte kurz warten!");
        final Integer[][] multiplicationTable = new Integer[10][10];
        for (int i = 1; i <= 10; i++)
        {
            for (int j = 1; j <= 10; j++)
            {
                multiplicationTable[i - 1][j - 1] = multi.multiply(i, j);
            }
        }
        RmiMultiplicationClient.printMultiplicationTable(multiplicationTable);
    }

    private static void printMultiplicationTable(final Integer[][] multiplicationTable)
    {
        System.out.println();
        System.out.printf("%3s|%3d|%3d|%3d|%3d|%3d|%3d|%3d|%3d|%3d|%3d", " ", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println();
        System.out.println("--------------------------------------------");
        for (int j = 0; j < multiplicationTable.length; j++)
        {
            System.out.printf("%3d|", j + 1);
            Integer[] intList = multiplicationTable[j];

            for (int i = 0; i < intList.length; i++)
            {
                System.out.printf("%3d ", intList[i]);
            }
            System.out.println();
        }
    }
}
