package da.tasks.rmi.mult;

import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;

public class MultiplicationImpl extends UnicastRemoteObject implements Multiplication
{

    /**
     * 
     */
    private static final long serialVersionUID = -6937581719421164387L;

    public MultiplicationImpl() throws RemoteException
    {
        // Empty
    }

    @Override
    public int multiply(int number1, int number2) throws RemoteException
    {
        return number1 * number2;
    }

    public String testClientHost() throws RemoteException, ServerNotActiveException
    {
        return RemoteServer.getClientHost();
    }

}
