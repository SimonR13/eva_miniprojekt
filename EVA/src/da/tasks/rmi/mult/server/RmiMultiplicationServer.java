package da.tasks.rmi.mult.server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.server.RemoteServer;

import da.tasks.rmi.mult.Multiplication;
import da.tasks.rmi.mult.MultiplicationImpl;

public class RmiMultiplicationServer
{
    public static void main(final String[] args) throws IOException, AlreadyBoundException
    {
        RemoteServer.setLog(new DataOutputStream(System.out));
        final MultiplicationImpl multi = new MultiplicationImpl();
        Naming.bind(Multiplication.DEFAULT_RMI_OBJECT_NAME, multi);
        System.out.println("Multiplikations-Server bereit");
    }
}
