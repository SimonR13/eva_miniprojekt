package da.tasks.rmi.mult;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;

public interface Multiplication extends Remote
{
    final String DEFAULT_RMI_OBJECT_NAME = "Multiplication";
    
    public abstract int multiply(int number1, int number2) throws RemoteException;
    
    public String testClientHost() throws RemoteException, ServerNotActiveException;
    
}
