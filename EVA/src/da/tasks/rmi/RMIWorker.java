package da.tasks.rmi;

import java.awt.EventQueue;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

import javax.swing.JOptionPane;

public class RMIWorker
{
    /** {@link ExecutorService} zur asynchronen Aufrühung der Aufrufe. */
    private static final ExecutorService EXECSERVICE = Executors.newCachedThreadPool();

    public interface RMIProducer<R>
    {
        R doRMICall() throws RemoteException;
    }

    public interface GUIConsumer<A>
    {
        void doGUIManipulation(final A argument);
    }

    public static <R, A> void doAsync(final RMIProducer<R> rmiProd, final Function<R, A> func, final GUIConsumer<A> guiCons)
    {
        // new Thread(() -> RMIWorker.doDirect(rmiProd, func, guiCons)).start();
        RMIWorker.EXECSERVICE.execute(() -> RMIWorker.doDirect(rmiProd, func, guiCons));
    }

    public static <R, A> void doDirect(final RMIProducer<R> rmiProd, final Function<R, A> func, final GUIConsumer<A> guiCons)
    {
        try
        {
            final R rmiReturnValue = rmiProd.doRMICall();
            EventQueue.invokeLater(() -> guiCons.doGUIManipulation(func.apply(rmiReturnValue)));

        }
        catch (final RemoteException re)
        {
            EventQueue.invokeLater(() -> JOptionPane.showMessageDialog(null, "Fehler: " + re.getMessage()));
        }
    }
}
