package da.tasks.rmi.bank;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IBank extends Remote
{
    public String DEFAULT_RMI_OBJECT_NAME = "BANK";
    
    void openAccount(final String acc) throws RemoteException;
    
}
