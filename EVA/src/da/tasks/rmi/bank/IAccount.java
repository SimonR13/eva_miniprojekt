package da.tasks.rmi.bank;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IAccount extends Remote
{

    double readBalance() throws RemoteException;

    void changeBalance(double valueToAdd) throws RemoteException;

}
