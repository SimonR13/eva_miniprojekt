package da.tasks.rmi.bank;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Bank extends UnicastRemoteObject implements IBank
{
    
    private final List<Account> kontos;

    protected Bank() throws RemoteException
    {
        kontos = new ArrayList<Account>();
        
        for (int i = 0; i < 100; i++) {
            kontos.add(new Account("konto" + (i+1), 0.0));
        }
    }

    public void openAccount(final String acc) throws RemoteException
    {
        
    }

}
