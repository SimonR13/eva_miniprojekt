package da.tasks.rmi.bank;

import java.rmi.RemoteException;

public class Account implements IAccount
{

    private String _name;

    private double _stand;

    public Account(final String name, final double stand) throws RemoteException
    {
        _name = name;
        _stand = stand;
    }

    public String getName()
    {
        return _name;
    }

    @Override
    public synchronized double readBalance() throws RemoteException
    {
        // TODO Auto-generated method stub
        return _stand;
    }

    @Override
    public synchronized void changeBalance(double valueToAdd) throws RemoteException
    {
        _stand = _stand + valueToAdd;
    }

}
