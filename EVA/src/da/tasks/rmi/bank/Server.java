package da.tasks.rmi.bank;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server
{

    public static void main(String[] args) throws RemoteException, AlreadyBoundException
    {
        final IBank bank = new Bank();
        final Registry localReg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        localReg.bind(IBank.DEFAULT_RMI_OBJECT_NAME, bank);
        System.out.println("Server ist bereit!");
    }

}
