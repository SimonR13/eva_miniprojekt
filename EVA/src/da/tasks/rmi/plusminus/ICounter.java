package da.tasks.rmi.plusminus;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICounter extends Remote
{
    public final String DEFAULT_RMI_OBJECT_NAME = "COUNTER";
    
    int increment() throws RemoteException;
    int decrement() throws RemoteException;
    
}
