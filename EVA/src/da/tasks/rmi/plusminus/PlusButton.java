package da.tasks.rmi.plusminus;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;

import da.lecture.rmi.RMIWorker;

@SuppressWarnings("serial")
public class PlusButton extends JButton
{
    private final ICounter _counter;
    private final JLabel _label;
    
    public PlusButton(ICounter counter, JLabel label) {
        
        super("Inkrement");
        this.addActionListener(this::onClicked);
        
        _counter = counter;
        _label = label;
    }

    private void onClicked(final ActionEvent event) {
        RMIWorker.doAsync(() -> _counter.increment(), newValue -> _label.setText(String.valueOf(newValue)));
    }
}
