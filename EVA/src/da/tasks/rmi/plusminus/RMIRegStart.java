package da.tasks.rmi.plusminus;

import java.awt.GridLayout;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

@SuppressWarnings("serial")
public class RMIRegStart extends JFrame
{
    
    protected final ICounter _counter;
    protected final CounterLabel _counterLabel;
    
    protected RMIRegStart(ICounter counter) {
        super("Counter Label&Buttons");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        
        _counter = counter;
        _counterLabel = new CounterLabel(_counter);
        _counterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        
        final PlusButton plusBtn = new PlusButton(_counter, _counterLabel);
        final MinusButton minusBtn = new MinusButton(_counter, _counterLabel);
        
        this.setLayout(new GridLayout(0, 1));
        this.add(plusBtn);
        this.add(this._counterLabel);
        this.add(minusBtn);
        this.setVisible(true);
    }
    
    

    public static void main(String[] args) throws RemoteException, MalformedURLException, AlreadyBoundException
    {
        final ICounter _counter = new CounterModel(0, 10, 1);
        Naming.bind(ICounter.DEFAULT_RMI_OBJECT_NAME, _counter);
        System.out.println("RMI-Registry started; Object exported; CounterModel(0,10,1)");
        new RMIRegStart(_counter);
    }

}
