package da.tasks.rmi.plusminus;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class CounterLabel extends JLabel
{
    
    private final ICounter _counter;

    public CounterLabel(final ICounter counter)
    {
        super();
        _counter = counter;
        
    }

    public ICounter get_counter()
    {
        return _counter;
    }

}
