package da.tasks.rmi.plusminus;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

@SuppressWarnings("serial")
public class CounterModel extends UnicastRemoteObject implements ICounter
{

    private static int _min, _max, _step;
    private static int counter;
    
    protected CounterModel(final int min, final int max, final int step) throws RemoteException
    {
        _min = min;
        _max = max;
        _step = step;
        counter = 0;
    }

    @Override
    public synchronized int increment() throws RemoteException
    {
        if (counter + _step <= _max) {
            counter = counter + _step;
        }
        return counter;
    }

    @Override
    public synchronized int decrement() throws RemoteException
    {
        if (counter - _step >= _min) {
            counter = counter - _step;
        }
        return counter;
    }

}
