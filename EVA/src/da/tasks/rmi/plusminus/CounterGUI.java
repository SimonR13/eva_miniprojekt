package da.tasks.rmi.plusminus;

import java.awt.GridLayout;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

@SuppressWarnings("serial")
public class CounterGUI extends JFrame
{
    protected final ICounter _counter;
    protected final CounterLabel _counterLabel;
    
    public CounterGUI(ICounter counter) {
        super("Counter Label&Buttons");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        
        _counter = counter;
        _counterLabel = new CounterLabel(_counter);
        _counterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        
        final PlusButton plusBtn = new PlusButton(_counter, _counterLabel);
        final MinusButton minusBtn = new MinusButton(_counter, _counterLabel);
        
        this.setLayout(new GridLayout(0, 1));
        this.add(plusBtn);
        this.add(this._counterLabel);
        this.add(minusBtn);
        this.setVisible(true);
    }
    
    

    public static void main(String[] args) throws RemoteException, NotBoundException
    {
        final Registry localReg = LocateRegistry.getRegistry();
        final ICounter counter = (ICounter) localReg.lookup(ICounter.DEFAULT_RMI_OBJECT_NAME);
        
        new CounterGUI(counter);
    }

}
