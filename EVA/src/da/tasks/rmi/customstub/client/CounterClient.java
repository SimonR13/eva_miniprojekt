package da.tasks.rmi.customstub.client;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.rmi.registry.Registry;

import da.tasks.rmi.customstub.server.Counter;

public class CounterClient
{
    public static void main(final String[] args) throws IOException
    {
        // final String objUrl = String.format("rmi://%s/%s", args[0],
        // Counter.DEFAULT_RMI_OBJECT_NAME);
        // final Counter counter = (Counter) Naming.lookup(objUrl);

        InetSocketAddress skeleton = new InetSocketAddress("localhost", Registry.REGISTRY_PORT);

        final CounterStub counterStub = new CounterStub(skeleton);

        final int incrementCount = Integer.parseInt(args[1]);
        CounterClient.playWithCounter(counterStub, incrementCount);
    }

    private static void playWithCounter(final CounterStub ctr, final int incrCnt)
    {
        Method reset = null, increment = null;
        try
        {
            reset = Counter.class.getMethod("reset", Counter.class);
            increment = Counter.class.getMethod("increment", Counter.class);
        }
        catch (NoSuchMethodException | SecurityException e)
        {
            e.printStackTrace();
        }
        if (reset != null)
        {
            int counterValueAfterReset;
            try
            {
                counterValueAfterReset = ctr.sendMethod(reset);
                System.out.println("Antwort: " + counterValueAfterReset);
            }
            catch (ClassNotFoundException | IOException e)
            {
                e.printStackTrace();
            }
        }
        if (increment != null)
        {
            try
            {

                for (int i = 0; i < incrCnt; i++)
                {
                    final int counterValueAfterIncrement = ctr.sendMethod(increment);
                    System.out.println("Antwort: " + counterValueAfterIncrement);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
