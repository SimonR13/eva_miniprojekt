package da.tasks.rmi.customstub.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;

public class CounterStub
{

    private final Socket socket;

    public CounterStub(final InetSocketAddress skeleton) throws IOException
    {
        socket = new Socket(skeleton.getAddress(), skeleton.getPort());
    }

    public int sendMethod(final Method method) throws IOException, ClassNotFoundException
    {
        int retVal = 0;
        if (this.socket != null)
        {
            try (final ObjectOutputStream objOS = new ObjectOutputStream(socket.getOutputStream());
            final ObjectInputStream objIS = new ObjectInputStream(socket.getInputStream()))
            {
                objOS.writeObject(method);

                retVal = (int) objIS.readObject();
            }
        }

        return retVal;
    }

}
