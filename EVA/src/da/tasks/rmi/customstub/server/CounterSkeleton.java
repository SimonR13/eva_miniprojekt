package da.tasks.rmi.customstub.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;

public class CounterSkeleton
{

    private static Counter counterObj;

    private static ServerSocket sSocket;

    public CounterSkeleton(final int port, final Counter object) throws IOException
    {
        if (CounterSkeleton.sSocket == null)
        {
            CounterSkeleton.sSocket = new ServerSocket(port);
        }

        new Thread(() -> {
            while (true)
            {
                try (final Socket socket = CounterSkeleton.sSocket.accept())
                {
                    CounterSkeleton.handleConnection(socket);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }).start();

        CounterSkeleton.counterObj = object;
    }

    private static void handleConnection(final Socket socket) throws IOException, ClassNotFoundException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        try (final ObjectInputStream objIS = new ObjectInputStream(socket.getInputStream());
        final ObjectOutputStream objOS = new ObjectOutputStream(socket.getOutputStream()))
        {

            int retVal = 0;
            final Method counterMethod = (Method) objIS.readObject();

            if (counterMethod != null)
            {
                retVal = (int) counterMethod.invoke(counterObj, new Object[]
                {});
            }
            objOS.writeObject(retVal);
        }
    }
}
