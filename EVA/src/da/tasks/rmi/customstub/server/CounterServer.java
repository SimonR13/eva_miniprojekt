package da.tasks.rmi.customstub.server;

import java.io.IOException;
import java.rmi.registry.Registry;

/**
 * Server-Klasse, die ein Objekt vom Typ {@link SynchronizedCounterImpl} bei
 * einer lokal laufenden RMI-Registry unter dem Namen
 * {@link Counter#DEFAULT_RMI_OBJECT_NAME} registriert.
 */
public class CounterServer
{
    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit der
     *             RMI-Registry zu einem Ein-/Ausgabefehler gekommen ist.
     * @throws AlreadyBoundException
     *             Wird ausgelöst, wenn unter dem Namen
     *             {@link Counter#DEFAULT_RMI_OBJECT_NAME} bereits ein anderes
     *             RMI-Objekt bei der RMI-Registry registriert ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final SynchronizedCounterImpl counter = new SynchronizedCounterImpl();
        new CounterSkeleton(Registry.REGISTRY_PORT, counter);
        
        System.out.println("Counter-Server bereit");
    }
}
