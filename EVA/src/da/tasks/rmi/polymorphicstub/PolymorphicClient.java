package da.tasks.rmi.polymorphicstub;

import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class PolymorphicClient
{
    public static void main(final String[] args) throws IOException, NotBoundException
    {
        final String objUrl = String.format("rmi://%s/%s", (args.length < 1 ? "localhost" : args[0]), PolymorphicClass.DEFAULT_RMI_OBJECT_NAME);
        final Object obj = Naming.lookup(objUrl);

        final PolymorphicInterfaceDown counterDown = (PolymorphicInterfaceDown) obj;
        final PolymorphicInterfaceUp counterUp = (PolymorphicInterfaceUp) obj;

        PolymorphicClient.playWithCounter(counterUp, counterDown);
    }

    private static void playWithCounter(final PolymorphicInterfaceUp counterUp, final PolymorphicInterfaceDown counterDown) throws RemoteException
    {
        System.out.println();
        System.out.println("CounterUp.countUp()");
        for (int i = 1; i < 100; i++)
        {
            System.out.println(i + ": " + counterUp.countUp());
        }
        System.out.println();
        System.out.println("CounterDown.countDown()");
        for (int i = 1; i < 100; i++)
        {
            System.out.println(i + ": " + counterDown.countDown());
        }
    }
}
