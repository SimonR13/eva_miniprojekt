package da.tasks.rmi.polymorphicstub;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PolymorphicInterfaceDown extends Remote
{

    int countDown() throws RemoteException;
    
}
