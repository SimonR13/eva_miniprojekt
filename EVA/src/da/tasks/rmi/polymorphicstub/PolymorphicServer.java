package da.tasks.rmi.polymorphicstub;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class PolymorphicServer
{

    public static void main(String[] args) throws RemoteException, MalformedURLException, AlreadyBoundException
    {
        final PolymorphicClass polyClass = new PolymorphicClass();
        Naming.bind(PolymorphicClass.DEFAULT_RMI_OBJECT_NAME, polyClass);
        System.out.println("PolyClass-Server is ready :D");

    }

}
