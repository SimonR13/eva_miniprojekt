package da.tasks.rmi.polymorphicstub;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PolymorphicInterfaceUp extends Remote
{

    int countUp() throws RemoteException;
    
}
