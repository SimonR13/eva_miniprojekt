package da.tasks.rmi.polymorphicstub;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class PolymorphicClass 
    extends UnicastRemoteObject 
    implements PolymorphicInterfaceDown, PolymorphicInterfaceUp
{
    public final static String DEFAULT_RMI_OBJECT_NAME = "polymorphicRMI";

    private int counter = 0;
    
    /**
     * 
     */
    private static final long serialVersionUID = -3209285252004479711L;

    protected PolymorphicClass() throws RemoteException
    {
        // nothing todo only for exception handling
    }

    @Override
    public int countDown() throws RemoteException
    {
        return counter--;
    }

    @Override
    public int countUp() throws RemoteException
    {
        return counter++;
    }

}
