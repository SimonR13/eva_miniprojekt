package da.tasks.lambda;

public interface IMoviePrinter
{

    void printMovie(final Movie movie);

}
