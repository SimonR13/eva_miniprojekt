package da.tasks.lambda;

public interface ISelector<T>
{
    
    Boolean select(final T obj);

}
