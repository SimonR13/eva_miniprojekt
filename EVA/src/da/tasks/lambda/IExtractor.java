package da.tasks.lambda;

public interface IExtractor<I, O> {
  O extractValue(final I input);
}
