package da.tasks.lambda;

import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;
import java.util.function.*;

public class Main
{
    public static void main(final String[] args)
    {
        final String arni = "Arnold Schwarzenegger";
        final String sly = "Sylvester Stallone";

        final Movie predator = new Movie("Predator", 1987, 107, arni, "Carl Weathers", "Bill Duke");
        final Movie commando = new Movie("Commando", 1985, 90, arni, "Rae Dawn Chong", "Dan Hedaya");
        final Movie cobra = new Movie("Cobra", 1986, 87, sly, "Brigitte Nielsen");
        final Movie demolitionMan = new Movie("Demolition Man", 1993, 115, sly, "Wesley Snipes", "Sandra Bullock");
        final Movie judgeDredd = new Movie("Judge Dredd", 1995, 96, sly, "Rob Schneider", "Jürgen Prochnow");
        final Movie snatch = new Movie("Snatch", 2000, 104, "Brad Pitt", "Benicio Del Toro", "Vinnie Jones", "Jason Statham");
        final Movie fightClub = new Movie("Fight Club", 1999, 139, "Brad Pitt", "Edward Norton", "Helena Bonham Carter");
        final Movie expendables = new Movie("The Expendables", 2010, 103, sly, "Jason Statham", "Dolph Lundgren", arni);

        final Collection<Movie> movies = Arrays.asList(predator, commando, cobra, demolitionMan, judgeDredd, snatch, fightClub, expendables);
        final Collection<String> stars = new TreeSet<>();
        for (final Movie curMovie : movies)
        {
            stars.addAll(curMovie.stars);
        }

        
          /*Aufgabenteil d */
          Main.print(movies, (movie) -> System.out.println(movie));
          
          Main.print(movies, System.err::println);
          
          Main.print(stars, (star) -> System.out.println(star));
          
          Main.print(stars, System.err::println);
         

        
         /* Aufgabenteil e*/
          
          Main.print(movies, (movie) -> movie.stars.contains(arni), 
                              (movie) -> System.out.println(movie));
          
          Main.print(stars, (star) -> star.contains("en"),
                              (star) -> System.out.println(star));
          
          Main.print(movies, (movie) -> movie.stars.contains(sly) && movie.releaseYear <= 2001,
                              (movie) -> System.out.println(movie));
         
        /*Aufgabenteil f/g*/
        Main.print(movies, (movie) -> movie.stars.contains(arni), 
                    (movie) -> movie.lengthInMinutes, 
                    (movie) -> System.out.println(movie));
        
        Main.print(stars, (star) -> star.contains("en"), 
                    (star) -> star.split(" ")[0], 
                    (star) -> System.out.println(star));
        
    }

//    private static <T> void printToSysout(final Collection<T> coll)
//    {
//        for (final T obj : coll)
//        {
//            System.out.println((T) obj);
//        }
//    }

//    private static void printMovies(final Collection<Movie> movies, final IMoviePrinter out)
//    {
//        for (final Movie movie : movies)
//        {
//            out.printMovie(movie);
//        }
//    }

    /*Aufgabenteil d*/
    private static <T> void print(final Collection<T> coll, final IPrinter<T> printer)
    {
        for (final T obj : coll)
        {
            printer.print(obj);
        }
    }

    /*Aufgabenteil e*/
    private static <T> void print(final Collection<T> coll, final ISelector<T> sel, final IPrinter<T> pnt)
    {
        for (final T obj : coll)
        {
            if (sel.select(obj))
            {
                pnt.print(obj);
            }
        }
    }
    
    /*Aufgabenteil f*/
//    private static <T, O> void print(final Collection<T> coll, final ISelector<T> sel, 
//                                   final IExtractor<T, O> ext , final IPrinter<O> pnt)
//    {
//        for (final T obj : coll)
//        {
//            if (sel.select(obj))
//            {
//                pnt.print(ext.extractValue(obj));
//            }
//        }
//    }
    
    /*Aufgabenteil g*/
    private static <T, O> void print(final Collection<T> coll, final Predicate<T> sel,
                                        final Function<T, O> ext, final Consumer<O> pnt)
    {
        for (final T obj : coll)
        {
            if (sel.test(obj))
            {
                pnt.accept(ext.apply(obj));
            }
        }
    }

//    private static void printMoviesToSysout(final Collection<Movie> movies)
//    {
//        for (final Movie curMovie : movies)
//        {
//            System.out.println(curMovie);
//        }
//    }

//    private static void printStarsToSysout(final Collection<String> stars)
//    {
//        for (final String star : stars)
//        {
//            System.out.println(star);
//        }
//    }
}
