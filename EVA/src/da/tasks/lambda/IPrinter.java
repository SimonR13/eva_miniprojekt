package da.tasks.lambda;

public interface IPrinter<T>
{

    void print(final T object);
    
}
