package da.lecture.socket.io.tcp.simpleexample;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Einfacher TCP-Client zur Veranschaulichung des prinzipiellen Aufbaus einer
 * TCP-Clientanwendung. Dazu wird die Zeichenkette "Hallo" UTF-8-kodiert an
 * einen TCP-Server verschickt, der auf Port 1250 unter einer per Kommandozeile
 * zu übergenden Adresse lauscht. Danach wird eine Antwort des Servers
 * abgewartet und diese wird auf der Konsole ausgegeben.
 */
public class SimpleClient
{
    /**
     * Hauptprogramm. Als Kommandozeilenargument muss die Adresse (IP-Adresse
     * oder DNS-Name) des Servers übergeben werden
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link Socket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final InetAddress server = InetAddress.getByName(args[0]); // IP-Adresse
                                                                   // od.
                                                                   // DNS-Name
        try (final Socket socket = new Socket(server, 1250))
        { // Verbindungsaufbau mit dem Server
            final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
            writer.write("Hallo!");
            writer.newLine();
            writer.flush();

            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
            final String answer = reader.readLine(); // Blockiert, bis ganze
                                                     // Zeile empfagen wurde
            System.out.println(answer);
        } // Verbindung mit Server wird abgebaut und Datenströme werden
          // automatisch geschlossen
    }
}
