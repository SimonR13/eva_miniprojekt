package da.lecture.socket.io.tcp.simpleexample;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Einfacher TCP-Server zur Veranschaulichung des prinzipiellen Aufbaus einer
 * TCP-Serveranwendung. Der Server horcht auf Port 1250 auf ankommende
 * Verbindungsanfragen. Die über angenommene Verbindungen ausgetauschenten Daten
 * werden als UTF-8-kodierte Zeichenektten auffasst. Vor eine solche
 * Zeichenektte wird die Zeichenkette "Selber " gehängt und das Ergebnis wird
 * UTF-8-kodiert an den Client zurückgesendet.
 */
public class SimpleServer
{
    /**
     * Hilfsmethode zur Abarbeitung einer Anfrage, die als Zeichenkette vom
     * übergebenen {@link Socket} gelesen wird.
     * 
     * @param socket
     *            {@link Socket}, von dem die Anfrage eines Clients gelesen und
     *            über das die Antwort an den anfragenden Client gesendet werden
     *            soll.
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link Socket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    private static void processConnection(final Socket socket) throws IOException
    {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
        final String request = reader.readLine(); // Blockiert, bis ganze Zeile
                                                  // empfagen wurde

        final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
        writer.write("Selber " + request);
        writer.newLine();
        writer.flush();
    }

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation über das
     *             verwendete {@link ServerSocket} beziehungsweise
     *             {@link Socket} zu einem Ein-/Ausgabefehler gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        try (final ServerSocket serverSocket = new ServerSocket(1250))
        {
            while (true)
            {
                try (final Socket socketToClient = serverSocket.accept())
                {
                    SimpleServer.processConnection(socketToClient);
                } // Verbindung mit Client wird abgebaut und Datenströme werden
                  // automatisch geschlossen
            }
        }
    }
}
