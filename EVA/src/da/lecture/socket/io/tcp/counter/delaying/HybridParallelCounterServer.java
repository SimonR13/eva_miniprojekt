package da.lecture.socket.io.tcp.counter.delaying;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * TCP-Server, der einen int-Zähler verwaltet und die Befehle "increment" und
 * "reset" versteht. Auf jeden Befehl wird der Zählerstand nach dem Befehl
 * geantwortet. Vor dem Antworten schindet der Server ein wenig Zeit um dem
 * Client vorzugaukeln, dass es sich um langandauernde Operationen handelt. Für
 * die Abarbeitung der Befehle wird eine bestimmte Anzahl an Threads verwendet,
 * die sich dynamisch in innerhalb des Intervalls [
 * {@link HybridParallelCounterServer#MIN_WORKER_COUNT},
 * {@link HybridParallelCounterServer#MAX_WORKER_COUNT}] bewegt.
 */
public class HybridParallelCounterServer
{
    /** Port, auf dem der TCP-Server auf Befehle lauscht. */
    public static final int DEFAULT_PORT = 1250;

    /**
     * Minimale Anzahl an Threads, die zur Abarbeitung von Verbindungen
     * verwendet werden.
     */
    public static final int MIN_WORKER_COUNT = 3;

    /**
     * Maximale Anzahl an Threads, die zur Abarbeitung von Verbindungen
     * verwendet werden.
     */
    public static final int MAX_WORKER_COUNT = 12;

    /**
     * Hauptprogramm.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Verbindungsannahme über das
     *             verwendete {@link ServerSocket} zu einem Ein-/Ausgabefehler
     *             gekommen ist.
     */
    public static void main(final String[] args) throws IOException
    {
        final SynchronizedCounter counter = new SynchronizedCounter();
        final ExecutorService execService = new ThreadPoolExecutor(HybridParallelCounterServer.MIN_WORKER_COUNT, HybridParallelCounterServer.MAX_WORKER_COUNT, 5, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(HybridParallelCounterServer.MAX_WORKER_COUNT * 2));

        try (final ServerSocket sSocket = new ServerSocket(HybridParallelCounterServer.DEFAULT_PORT))
        {
            while (true)
            {
                final Socket socketToClient = sSocket.accept();
                final CounterServerRunnable runnable = new CounterServerRunnable(socketToClient, counter);
                execService.execute(runnable);
            }
        }
    }
}
