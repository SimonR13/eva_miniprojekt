package da.lecture.socket.io.tcp.pop;

import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.swing.JOptionPane;

/**
 * Klasse, deren main-Methode unter Anwendung des POP-Protokolls E-Mails von
 * einem Server abuft. Die Adresse des E-Mail-Servers und der zur Anmeldung zu
 * verwendende Benutzername müssen als Kommandozeilenargumente übergeben werden.
 * Das Passwort wird dann zur Laufzeit durch einen Dialog erfragt. Zum
 * tatsächlichen Kommunikation mit dem E-Mail-Server wird die Java-Mail-API
 * verwendet (http://www.oracle.com/technetwork/java/javamail/index.html).
 */
public class POPFetcher
{
    /**
     * Hauptprogramm. Als Kommandozeilenargumente müssen die Adresse des
     * E-Mail-Servers und der zur Anmeldung zu verwendende Benutzername
     * angegeben weden.
     * 
     * @param args
     *            Kommandozeilenargumente.
     * @throws Exception
     *             Wird ausgelöst, wenn es bei der Kommunikation mit dem
     *             E-Mail-Server zu einem Ein-/Ausgabefehler gekommen ist.
     */
    public static void main(final String[] args) throws Exception
    {
        final Session session = Session.getInstance(new Properties());
        final Store popStore = session.getStore(new URLName("pop3://" + args[0]));
        popStore.connect(args[1], JOptionPane.showInputDialog("Passwort für " + args[1]));

        final Folder defaultFolder = popStore.getFolder("INBOX");
        defaultFolder.open(Folder.READ_ONLY);
        for (final Message currentMessage : defaultFolder.getMessages())
        {
            POPFetcher.printMessage(currentMessage);
        }
    }

    /**
     * Hilfsmethode, die bestimmte Inhalte der übergebenen E-Mail-Nachricht auf
     * der Konsole ausgibt.
     * 
     * @param messageToPrint
     *            E-Mail-Nachricht, deren Inhalte ausgegeben werden sollen.
     * @throws Exception
     *             Wird ausgelöst, wenn es bei der Kommunikation mit dem
     *             E-Mail-Server zu einem Ein-/Ausgabefehler gekommen ist.
     */
    private static void printMessage(final Message messageToPrint) throws Exception
    {
        System.out.println("Von: " + messageToPrint.getFrom()[0]);
        System.out.println("Betreff: " + messageToPrint.getSubject());
        System.out.println(messageToPrint.getContent());
        System.out.println();
    }
}
