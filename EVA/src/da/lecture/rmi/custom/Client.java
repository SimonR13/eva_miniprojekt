package da.lecture.rmi.custom;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import da.lecture.rmi.exportexamination.SomeRMIInterface;
import da.lecture.rmi.exportexamination.SomeRMIInterfaceImpl;

/**
 * Client zur Veranschaulichung der Verwendung der eigenen Stub- und Skeleton-Implementierung aus {@link CustomSkeleton} und
 * {@link CustomStubInvocationHandler}.
 */
public class Client {
    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws RemoteException Wird ausgelöst, wenn es zu einem RMI spezifischen Fehler gekommen ist.
     * @throws NotBoundException Wird ausgelöst, wenn in einer Registry kein {@link SomeRMIInterface} an den Namen
     *             {@link SomeRMIInterface#DEFAULT_RMI_OBJECT_NAME} gebunden ist.
     */
    public static void main(final String[] args) throws RemoteException, NotBoundException {
        final Registry localReg = LocateRegistry.getRegistry();
        final SomeRMIInterface stub = (SomeRMIInterface) localReg.lookup(SomeRMIInterface.DEFAULT_RMI_OBJECT_NAME);

        final SomeRMIInterfaceImpl someOtherObj = new SomeRMIInterfaceImpl();
        stub.someMethod(someOtherObj);
    }
}
