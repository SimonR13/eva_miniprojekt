package da.lecture.rmi.custom;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ObjID;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementierung eines sequenziellen TCP-Servers, der entfernte Aufrufe auf Objekten ermöglicht, die über
 * {@link CustomSkeleton#export(Remote)} exportiert wurden.
 */
public class CustomSkeleton {
    /** Exportierte Objekte mit ihren Objektkennungen. */
    private static final Map<ObjID, Remote> EXOBJ = new HashMap<>();
    /** ServerSocket, auf dem Verbindungen von Stubs angenommen werden. */
    private static ServerSocket sSock;

    /**
     * Exportiert das übergebene Objekt, so dass nach Rückkehr aus der Methode über den zurückgelieferten Stub entfernte
     * Methodenaufrufe auf dem Objekt druchgeführt werden können.
     * @param obj Zu exportierendes Objekt.
     * @return Stub zum exportierten Objekt.
     * @throws RemoteException Wird ausgelöst, wenn es beim Exportieren zu einem Problem gekommen ist.
     */
    public static Remote export(final Remote obj) throws RemoteException {
        final InetSocketAddress skelAddr = CustomSkeleton.initSkeletonIfNotAlreadyDone();

        final ObjID newObjId = new ObjID(); // Konstruktor erzeugt zufällige, eindeutige Id
        CustomSkeleton.EXOBJ.put(newObjId, obj);

        final Class<?>[] remoteInterfaces = CustomSkeleton.findRemoteInterfaces(obj);
        final Class<?> stubClass = Proxy.getProxyClass(CustomSkeleton.class.getClassLoader(), remoteInterfaces);

        try {
            final InvocationHandler proxyHandler = new CustomStubInvocationHandler(newObjId, skelAddr);
            final Constructor<?> stubConst = stubClass.getConstructor(InvocationHandler.class);
            final Object stub = stubConst.newInstance(proxyHandler);

            return (Remote) stub;

        } catch (final ReflectiveOperationException roe) {
            // Würde ausgelöst, wenn beim Erzeugen des dynamischen Proxies / Stubs was schief läuft
            throw new RemoteException("Fehler bei Stub-Erzeugung", roe);
        }
    }

    /**
     * Hilfsmethode, die beim ersten Aufruf das {@link ServerSocket} in {@link #sSock} auf Port 4711 öffnet und einen Thread zur
     * Verbindungsannahme startet.
     * @return Adresse und Port des {@link ServerSocket}s aus {@link #sSock}.
     * @throws RemoteException Wird ausgelöst, wenn es beim Öffnen des {@link ServerSocket}s zu einem Problem gekommen ist.
     */
    private static InetSocketAddress initSkeletonIfNotAlreadyDone() throws RemoteException {
        if (CustomSkeleton.sSock == null) {
            try {
                CustomSkeleton.sSock = new ServerSocket(4711);

                final Thread acceptorThread = new Thread() {
                    @Override public void run() {
                        CustomSkeleton.acceptStubConnections();
                    }
                };
                acceptorThread.start();
            } catch (final IOException ioe) {
                throw new RemoteException("Fehler bei Initialisierung", ioe);
            }
        }

        return (InetSocketAddress) CustomSkeleton.sSock.getLocalSocketAddress();
    }

    /**
     * Hilfsmethode, die Verbindungen auf {@link #sSock} annimmt und dann mit jeder angenommenen Verbindung die Methode
     * {@link #processStubConnection(Socket)} aufruft.
     */
    private static void acceptStubConnections() {
        // Methode ist jetzt hier als sequenzieller Server aufgebaut. Könnte aber mit den bereits
        // bekannten Aufbauten relativ einfach in einen dynamisch oder hybrid parallelen Server
        // umgeschrieben werden.

        while (true) {
            try (final Socket sock = CustomSkeleton.sSock.accept()) {
                CustomSkeleton.processStubConnection(sock);

            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Methode, die die Verbindung zu einem Stub abarbeitet. Es wird erwartet, dass der Stub zuerst die {@link ObjID} des Objektes
     * schickt, auf dem eine Methode aufgerufen werden soll. Danach werden der Name der aufzurufenden Methode und die Argumente
     * für den Aufruf erwartet. Sobald der Methodenaufruf durchgeführt wurde, wird dem Stub mit dem Methodenrückgabewert
     * geantwortet.
     * @param s {@link Socket}, das die Verbindung zum Stub repräsentiert.
     * @throws Exception Wird ausgelöst, wenn irgendwie irgendwas schief gelaufen ist.
     */
    private static void processStubConnection(final Socket s) throws Exception {
        // Stub schickt in dieser Implementierung pro Verbindung immer nur die Daten für
        // einen Methodenaufruf. Daher ist hier keine Schleife notwendig.

        final ObjectInputStream in = new ObjectInputStream(s.getInputStream());
        final ObjID objId = (ObjID) in.readObject();
        final Remote obj = CustomSkeleton.EXOBJ.get(objId);

        final String mName = in.readUTF();
        final Class<?>[] mParamTypes = (Class<?>[]) in.readObject();
        final Method mToCall = obj.getClass().getMethod(mName, mParamTypes);

        final Object[] callArgs = (Object[]) in.readObject();
        final Object returnValue = mToCall.invoke(obj, callArgs);

        final ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
        out.writeObject(returnValue);
    }

    //
    //

    /**
     * Hilfsmethode, die alle von der Klasse des übergebenen Objektes implemenierten Interfaces nach solchen Interfaces
     * durchforstet, die entweder {@link Remote} sind oder aus {@link Remote} abgeleitet wurden.
     * @param obj Objekt, für das gesucht werden soll.
     * @return Siehe Methodenbeschreibung.
     */
    private static Class<?>[] findRemoteInterfaces(final Remote obj) {
        final Class<?>[] allImplementedInterfaces = obj.getClass().getInterfaces();
        final List<Class<?>> implementedRemoteInterfaces = new ArrayList<>();

        for (final Class<?> currentImplementedInterface : allImplementedInterfaces) {
            if (Remote.class.isAssignableFrom(currentImplementedInterface)) {
                implementedRemoteInterfaces.add(currentImplementedInterface);
            }
        }
        return implementedRemoteInterfaces.toArray(new Class<?>[0]);
    }
}
