package da.lecture.rmi.lamda;

import java.util.Arrays;
import java.util.Comparator;

public class ExcursusComparator {
    public static void main(final String[] args) {
        Arrays.sort(args, new Comparator<String>() {
            @Override public int compare(final String o1, final String o2) {
                return o1.length() - o2.length();
            }
        });

        Arrays.sort(args, (final String o1, final String o2) -> o1.length() - o2.length());
        Arrays.sort(args, (o1, o2) -> o1.length() - o2.length());
        Arrays.sort(args, (o1, o2) -> ExcursusComparator.compareStringLengths(o1, o2));
        Arrays.sort(args, ExcursusComparator::compareStringLengths);
    }

    private static int compareStringLengths(final String s1, final String s2) {
        return s1.length() - s2.length();
    }
}
