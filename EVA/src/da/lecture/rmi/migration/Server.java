package da.lecture.rmi.migration;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Server-Klasse, die ein Objekt vom Typ {@link MigratorImpl} bei einer lokal gestarteten RMI-Registry unter dem Namen
 * {@link Migrator#DEFAULT_RMI_OBJECT_NAME} registriert.
 */
public class Server {
    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws RemoteException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry oder beim Exportieren der
     *             {@link MigratorImpl}-Instanz zu einem Fehler gekommen ist.
     * @throws AlreadyBoundException Wird ausgelöst, wenn unter dem Namen {@link Migrator#DEFAULT_RMI_OBJECT_NAME} bereits ein
     *             anderes RMI-Objekt bei der RMI-Registry registriert ist.
     */
    public static void main(final String[] args) throws RemoteException, AlreadyBoundException {
        final Migrator migrator = new MigratorImpl();
        final Registry localReg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        localReg.bind(Migrator.DEFAULT_RMI_OBJECT_NAME, migrator);
        System.out.println("Migrator-Server bereit");
    }
}
