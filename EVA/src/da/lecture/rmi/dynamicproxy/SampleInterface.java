package da.lecture.rmi.dynamicproxy;

/**
 * Sinnfreie Beispielschnittstelle zur Veranschaulichung von dynamischen Proxies.
 */
public interface SampleInterface {
    /**
     * Sinnfreie Demonstrationsmethode 1.
     */
    void someMethod();

    /**
     * Sinnfreie Demonstrationsmethode 2.
     * @param param Toller Zeichenkettenparameter.
     * @return Irgend eine tolle Zeichenkette.
     */
    String someOtherMethod(String param);
}
