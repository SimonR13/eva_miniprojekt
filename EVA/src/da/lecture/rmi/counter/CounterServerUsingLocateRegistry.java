package da.lecture.rmi.counter;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteServer;

/**
 * Server-Klasse, die zuerst eine lokale RMI-Registry startet und dann ein Objekt vom Typ {@link SynchronizedCounterImpl} bei
 * dieser unter dem Namen {@link Counter#DEFAULT_RMI_OBJECT_NAME} registriert.
 */
public class CounterServerUsingLocateRegistry {
    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     * @throws AlreadyBoundException Wird ausgelöst, wenn unter dem Namen {@link Counter#DEFAULT_RMI_OBJECT_NAME} bereits ein
     *             anderes RMI-Objekt bei der RMI-Registry registriert ist.
     */
    public static void main(final String[] args) throws IOException, AlreadyBoundException {
        final SynchronizedCounterImpl conuter = new SynchronizedCounterImpl();
        final Registry localReg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        localReg.bind(Counter.DEFAULT_RMI_OBJECT_NAME, conuter);
        System.out.println("Counter-Server bereit");

        RemoteServer.setLog(System.out);
    }
}
