package da.lecture.rmi.callby.reference.semaphore;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Implementierung von {@link RMISemaphore}, in der sämtliche Funktionalität für die Methoden aus {@link RMISemaphore} selbst
 * programmiert ist.
 */
public class RMISemaphoreImpl extends UnicastRemoteObject implements RMISemaphore {
    /** Anzahl aktuell vorhandener Genehmigungen. */
    private int permits;

    /**
     * Initialisiert eine neue RMISemaphoreImpl-Instanz mit den übergebenen Argumenten.
     * @param initialPermits Initial vorhandene Anzahl an Genehmigungen
     * @throws RemoteException Wird ausgelöst, wenn es beim Exportieren des RMI-Objektes, also beim Erzeugen des Skeletons, zu
     *             einem Fehler gekommen ist.
     */
    public RMISemaphoreImpl(final int initialPermits) throws RemoteException {
        this.permits = Math.max(0, initialPermits);
    }

    @Override public synchronized void acquire() throws InterruptedException {
        while (this.permits == 0) {
            this.wait();
        }

        this.permits--;
    }

    @Override public synchronized void release() {
        this.permits++;
        this.notify();
    }
}
