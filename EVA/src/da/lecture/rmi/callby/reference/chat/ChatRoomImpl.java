package da.lecture.rmi.callby.reference.chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Implementierung von {@link ChatRoom}, die einen über RMI operierenden Chat-Raum nach dem Entwurfsmuster Oberserver zur
 * Verfügung stellt.
 */
public class ChatRoomImpl extends UnicastRemoteObject implements ChatRoom {
    /** Liste aller Benutzer dieses Chat-Raumes. */
    private final List<User> users;

    /**
     * Initialisiert eine neue ChatRoomImp-Instanz.
     * @throws RemoteException Wird ausgelöst, wenn es beim Exportieren des RMI-Objektes, also beim Erzeugen des Skeletons, zu
     *             einem Fehler gekommen ist.
     */
    public ChatRoomImpl() throws RemoteException {
        this.users = new ArrayList<>();
    }

    @Override public synchronized void addUser(final User newUser) throws RemoteException,
            UserWithNameAlreadyAddedException {
        final String newUserName = newUser.getName();
        final Iterator<User> userIter = this.users.iterator();

        User currentUser = null;
        while (userIter.hasNext()) {
            currentUser = userIter.next();

            try {
                if (currentUser.getName().equals(newUserName)) {
                    throw new UserWithNameAlreadyAddedException();
                }
            } catch (final RemoteException re) {
                // Es scheint ein Kommunikationsproblem mit dem gerade betrachteten Chatter zu geben. Es kann davon ausgegangen
                // werden, dass die Verbindung zu diesem Chatter gestört ist. Daher wird er aus der Liste der Benutzer dieses
                // Chat-Raumes entfernt.
                userIter.remove();
            }
        }

        this.users.add(newUser);
    }

    @Override public synchronized void removeUser(final User user) {
        this.users.remove(user);
    }

    @Override public synchronized void sendMessage(final String name, final String message) {
        final String messageToPrint = name + ": " + message;
        final Iterator<User> userIter = this.users.iterator();

        User currentUser = null;
        while (userIter.hasNext()) {
            currentUser = userIter.next();

            try {
                currentUser.printMessage(messageToPrint);
            } catch (final RemoteException re) {
                // Es scheint ein Kommunikationsproblem mit dem gerade betrachteten Chatter zu geben. Es kann davon ausgegangen
                // werden, dass die Verbindung zu diesem Chatter gestört ist. Daher wird er aus der Liste der Benutzer dieses
                // Chat-Raumes entfernt.
                userIter.remove();
            }
        }
    }
}
