package da.lecture.rmi.callby.value;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Server-Klasse, die zuerst eine lokale RMI-Registry startet und dann ein Objekt vom Typ {@link RemoteAppenderImpl} bei dieser
 * unter dem Namen {@link RemoteAppender#DEFAULT_RMI_OBJECT_NAME} registriert.
 */
public class Server {
    /**
     * Hauptprogramm.
     * @param args Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws IOException Wird ausgelöst, wenn es bei der Kommunikation mit der RMI-Registry zu einem Ein-/Ausgabefehler gekommen
     *             ist.
     * @throws AlreadyBoundException Wird ausgelöst, wenn unter dem Namen {@link RemoteAppender#DEFAULT_RMI_OBJECT_NAME} bereits
     *             ein anderes RMI-Objekt bei der RMI-Registry registriert ist.
     */
    public static void main(final String[] args) throws IOException, AlreadyBoundException {
        final RemoteAppender appender = new RemoteAppenderImpl();
        final Registry localReg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        localReg.bind(RemoteAppender.DEFAULT_RMI_OBJECT_NAME, appender);
        System.out.println("List-Server bereit");
    }
}
