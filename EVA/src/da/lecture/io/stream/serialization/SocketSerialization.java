package da.lecture.io.stream.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;
import java.net.Socket;

/**
 * Klasse zur Veranschaulichung der Wirkung des Schlüsselwortes transient im
 * Zusammenhang mit der Serialisierung über Datenströme vom Typ
 * {@link ObjectOutputStream} und {@link ObjectInputStream}.
 */
public class SocketSerialization implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -5648637042716682046L;

    /** Name der Datei, die für die Serialisierung verwendet werden soll. */
    public static final String FNAME = "seriTest.binary";

    /** Nicht-transientes Attribut. */
    @SuppressWarnings("unused")
    private int x;

    /** Transientes Attribut. */
    private transient Socket soc;

    /**
     * Initialisiert eine neue SocketSerialization-Instanz mit den übergebenen
     * Argumenten.
     * 
     * @param x
     *            Wert für das nicht-transiente Attribut.
     * @param hostName
     *            Name des Rechners, zu dem eine TCP-Verbindung auf Port 80
     *            hergestellt werden soll.
     * @throws IOException
     *             Wird ausgelöst, wenn es beim Aufbau der TCP-Verbindung zu
     *             einem Ein-/Ausgabefehler gekommen ist.
     */
    public SocketSerialization(final int x, final String hostName) throws IOException
    {
        this.x = x;
        this.soc = new Socket(hostName, 80);
    }

    /**
     * Fragt die Ressource index.html von dem Server ab, zu dem innerhalb des
     * Konstruktors eine TCP-Verbindung aufgebaut wurde.
     * 
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Kommunikation mit dem Server
     *             zu einem Ein-/Ausgabefehler gekommen ist.
     */
    public void printResonse() throws IOException
    {
        try (final Writer w = new OutputStreamWriter(this.soc.getOutputStream());
        final Reader r = new InputStreamReader(this.soc.getInputStream()))
        {
            w.write("GET /index.html HTTP/1.0\n");
            w.write("Host: " + this.soc.getInetAddress().getHostName() + "\n");
            w.write("\n");
            w.flush();

            final char[] charBuff = new char[128];
            int readChars = 0;
            while ((readChars = r.read(charBuff)) != -1)
            {
                System.out.print(String.valueOf(charBuff, 0, readChars));
            }
        }
    }

    /**
     * Methode zur Beeinflussung der Serialisierung durch einen
     * {@link ObjectOutputStream}.
     * 
     * @param s
     *            {@link ObjectOutputStream}, in den Serialisiert werden soll.
     * @throws IOException
     * @throws IOException
     *             Wird ausgelöst, wenn es bei der Serialisierung zu einem
     *             Fehler gekommen ist.
     */
    private void writeObject(final ObjectOutputStream s) throws IOException
    {
        s.defaultWriteObject();

        s.writeUTF(this.soc.getInetAddress().getHostName());
        s.writeInt(this.soc.getPort());
    }

    /**
     * Methode zur Beeinflussung der Deserialisierung durch einen
     * {@link ObjectInputStream}.
     * 
     * @param s
     *            {@link ObjectInputStream}, aus dem heraus Deserialisiert
     *            werden soll.
     * @throws Exception
     *             Wird ausgelöst, wenn es bei der Deserialisierung zu einem
     *             Fehler gekommen ist.
     */
    private void readObject(final ObjectInputStream s) throws Exception
    {
        s.defaultReadObject();

        final String hostName = s.readUTF();
        final int port = s.readInt();
        this.soc = new Socket(hostName, port);
    }

    /**
     * Hauptprogramm, welches zunächst ein {@link SocketSerialization}-Objekt
     * erzeugt. Dabei wird eine TCP-Verbindung zu dem Rechner www.google.de auf
     * Portnummer 80 aufgebaut. Danach wird dieses Objekt serialisiert. Da ein
     * {@link Socket}-Objekt nicht serialisiert werden kann, werden statt dessen
     * Rechnername und Portnummer serialisiert, zu dem die TCP-Verbindung zum
     * Zeitpunkt der Serialisierung bestand. Nach wird das Objekt wieder
     * Deserialisiert, wobei die deserialisierten Angaben von Rechnername und
     * Portnummer dazu verwendet werden, die TCP-Verbindung erneut aufzubauen.
     * Danach wird über die bei der Deserialisierung aufgebaute TCP-Verbindung
     * eine Anfrage an den Server gesendet und die Antwort auf der Konsole
     * ausgegeben.
     * 
     * @param args
     *            Kommandozeilenargumente (werden hier nicht benötigt).
     * @throws Exception
     *             Wird ausgelöst, wenn es bei der Ausführung des Beispiels zu
     *             einem Fehler gekommen ist.
     */
    public static void main(final String[] args) throws Exception
    {
        final SocketSerialization transientSerTest = new SocketSerialization(10, "www.google.de");

        try (final ObjectOutputStream oOut = new ObjectOutputStream(new FileOutputStream(SocketSerialization.FNAME));
        final ObjectInputStream oIn = new ObjectInputStream(new FileInputStream(SocketSerialization.FNAME)))
        {
            oOut.writeObject(transientSerTest);

            final SocketSerialization readObject = (SocketSerialization) oIn.readObject();
            readObject.printResonse();
        }
    }
}
